package de.luh.se.fmse.lts.visualizer;

import java.util.ArrayList;

import org.eclipse.jface.viewers.ISelection;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.jface.viewers.LabelProvider;
import org.eclipse.jface.viewers.Viewer;
import org.eclipse.swt.SWT;
import org.eclipse.swt.graphics.Color;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Display;
import org.eclipse.ui.ISelectionListener;
import org.eclipse.ui.IWorkbenchPart;
import org.eclipse.ui.part.ViewPart;
import org.eclipse.zest.core.viewers.GraphViewer;
import org.eclipse.zest.core.viewers.IGraphEntityRelationshipContentProvider;
import org.eclipse.zest.core.viewers.ISelfStyleProvider;
import org.eclipse.zest.core.widgets.GraphConnection;
import org.eclipse.zest.core.widgets.GraphNode;
import org.eclipse.zest.core.widgets.ZestStyles;
import org.eclipse.zest.layouts.LayoutStyles;
import org.eclipse.zest.layouts.algorithms.TreeLayoutAlgorithm;

import de.luh.se.fmse.lts.LTS;
import de.luh.se.fmse.lts.NamedElement;
import de.luh.se.fmse.lts.State;
import de.luh.se.fmse.lts.Transition;

public class LTSVisualizer extends ViewPart {


	public static final String ID = "de.luh.se.fmse.lts.visualizer";
	private GraphViewer viewer;

	// currently selected LTS, may change if selection in Eclipse changes.
	private LTS currentLTS;
	
	public LTS getCurrentLTS() {
		return currentLTS;
	}

	public void setCurrentLTS(LTS currentLTS) {
		this.currentLTS = currentLTS;
	}

	// method that must be implemented by every IWorkbenchPart. The workbench calls this method when the view is created/opened. 
	public void createPartControl(Composite parent) {
		
		//creating and configuring the graph viewer. 
		//Setting (1)style, (2)label and (3)content provider. Also setting (4)layout algorithm, (5) input model. 
		viewer = new GraphViewer(parent, SWT.NONE);
		viewer.setConnectionStyle(ZestStyles.CONNECTIONS_DIRECTED);
		viewer.setLabelProvider(new LTSLabelProvider());
		LTSContentProvider ltsContentProvider = new LTSContentProvider();
		viewer.setContentProvider(ltsContentProvider);
		viewer.setLayoutAlgorithm(new TreeLayoutAlgorithm(LayoutStyles.NO_LAYOUT_NODE_RESIZING), true);
		viewer.setInput(currentLTS);
		viewer.applyLayout();
		
		hookSelectionListener();
	}

	protected void hookSelectionListener() {
		// hooking selection listener into workbench window's selection service
		getSite().getWorkbenchWindow().getSelectionService()
				.addSelectionListener(selectionListener);
	}
	private void disposeSelectionListener() {
		getSite().getWorkbenchWindow().getSelectionService()
				.removeSelectionListener(selectionListener);
	}
	
	protected GraphViewer getViewer() {
		return viewer;
	}

	@Override
	public void setFocus() {
		// TODO Auto-generated method stub

	}
	@Override
	public void dispose() {
		disposeSelectionListener();
		super.dispose();
	}

	// selection listener: when the user selects different elements in the workbench window, the listener is notified.
	// if the selected element is an instance of de.luh.se.fmse.lts.LTS, we change the currentLTS and refresh the view.
	private ISelectionListener selectionListener = new ISelectionListener() {

		@Override
		public void selectionChanged(IWorkbenchPart part, ISelection selection) {
			if (selection instanceof IStructuredSelection) {
				Object firstElement = ((IStructuredSelection) selection)
						.getFirstElement();
				if (firstElement instanceof LTS) {
					selectionChanged((LTS) firstElement);
				}
			}
			LTSVisualizer.this.getViewer().refresh();
		}

		private void selectionChanged(LTS lts) {
			setCurrentLTS(lts);
			getViewer().setInput(lts);
			getViewer().refresh();
		}

	};


	public class LTSContentProvider implements IGraphEntityRelationshipContentProvider{

		@Override
		public Object[] getElements(Object inputElement) {
			if(inputElement instanceof LTS) 
				return ((LTS)inputElement).getStates().toArray();
			else
				return new Object[0];
		}

		@Override
		public void dispose() {
			// TODO Auto-generated method stub
			
		}

		@Override
		public void inputChanged(Viewer viewer, Object oldInput, Object newInput) {
			// TODO Auto-generated method stub
			
		}

		@Override
		public Object[] getRelationships(Object source, Object dest) {
			if(source instanceof State && dest instanceof State){
				ArrayList<Transition> transitionsList = new ArrayList<Transition>();
				State sourceState = (State) source;
				State targetState = (State) dest;
				for (Transition transition : sourceState.getOutgoingTransitions()) {
					if (transition.getTargetState() == targetState)
						transitionsList.add(transition);
				}
				return transitionsList.toArray();
			}
			return null;
		}
	}

	public class LTSLabelProvider extends LabelProvider implements ISelfStyleProvider{
		
		@Override
		public String getText(Object element) {
			if (element instanceof Transition)
				if (((Transition)element).getLabel() == null)
					return "<no event label>";
				else
					return getText(((Transition)element).getLabel());
			else
			if (element instanceof NamedElement)
				if (((NamedElement)element).getName() != null && !((NamedElement)element).getName().isEmpty())
					return ((NamedElement)element).getName();
			return super.getText(element);
		}

		@Override
		public void selfStyleConnection(Object element,
				GraphConnection connection) {
			if (element instanceof Transition) {
				//Transition transition = (Transition) element;
				connection.setLineColor(new Color(Display.getCurrent(), 100, 100, 100));
			}			
		}

		@Override
		public void selfStyleNode(Object element, GraphNode node) {
			if (element instanceof State) {
				node.setBorderColor(new Color(Display.getCurrent(), 100, 100, 100));
				State state = (State) element;
				if (state.equals(state.getContainingLTS().getStartState()))
					node.setBorderWidth(3);
				else
					node.setBorderWidth(1);
			}						
		}


	}

}
