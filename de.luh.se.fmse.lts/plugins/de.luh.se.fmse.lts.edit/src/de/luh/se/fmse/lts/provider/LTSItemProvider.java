/**
 */
package de.luh.se.fmse.lts.provider;


import java.util.Collection;
import java.util.List;

import org.eclipse.emf.common.notify.AdapterFactory;
import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.util.BasicEList;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EStructuralFeature;
import org.eclipse.emf.edit.provider.ComposeableAdapterFactory;
import org.eclipse.emf.edit.provider.IItemPropertyDescriptor;
import org.eclipse.emf.edit.provider.ItemPropertyDescriptor;
import org.eclipse.emf.edit.provider.ViewerNotification;

import de.luh.se.fmse.lts.LTS;
import de.luh.se.fmse.lts.LtsFactory;
import de.luh.se.fmse.lts.LtsPackage;
import de.luh.se.fmse.lts.State;

/**
 * This is the item provider adapter for a {@link de.luh.se.fmse.lts.LTS} object.
 * <!-- begin-user-doc -->
 * <!-- end-user-doc -->
 * @generated
 */
public class LTSItemProvider extends NamedElementItemProvider {
	/**
	 * This constructs an instance from a factory and a notifier.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public LTSItemProvider(AdapterFactory adapterFactory) {
		super(adapterFactory);
	}

	/**
	 * This returns the property descriptors for the adapted class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public List<IItemPropertyDescriptor> getPropertyDescriptors(Object object) {
		if (itemPropertyDescriptors == null) {
			super.getPropertyDescriptors(object);

			addStartStatePropertyDescriptor(object);
		}
		return itemPropertyDescriptors;
	}

	/**
	 * This adds a property descriptor for the Start State feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	protected void addStartStatePropertyDescriptor(Object object) {
		itemPropertyDescriptors.add
//			(createItemPropertyDescriptor
//				(((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
//				 getResourceLocator(),
//				 getString("_UI_LTS_startState_feature"),
//				 getString("_UI_PropertyDescriptor_description", "_UI_LTS_startState_feature", "_UI_LTS_type"),
//				 LtsPackage.Literals.LTS__START_STATE,
//				 true,
//				 false,
//				 true,
//				 null,
//				 null,
//				 null));
		
		
		(
				 new ItemPropertyDescriptor(
						 ((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
						 getResourceLocator(),
						 getString("_UI_LTS_startState_feature"),
						 getString("_UI_PropertyDescriptor_description", "_UI_LTS_startState_feature", "_UI_LTS_type"),
						 LtsPackage.Literals.LTS__START_STATE,
						 true,
						 null,
						 null,
						 null)
						 {
					 		public Collection<?> getChoiceOfValues(Object object){

					 			try{
						 			EList<State> choiceValueList = new BasicEList<State>();
						 			
						 			LTS lts = (LTS)object;

						 			choiceValueList.addAll(lts.getStates());
						 			
						 			return choiceValueList;
								} catch (NullPointerException e) {
									//nothing to worry about there. May occur if alphabet is not created yet.
								}
								return super.getChoiceOfValues(object);
					 		}

						 }
				);

	}

	/**
	 * This specifies how to implement {@link #getChildren} and is used to deduce an appropriate feature for an
	 * {@link org.eclipse.emf.edit.command.AddCommand}, {@link org.eclipse.emf.edit.command.RemoveCommand} or
	 * {@link org.eclipse.emf.edit.command.MoveCommand} in {@link #createCommand}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Collection<? extends EStructuralFeature> getChildrenFeatures(Object object) {
		if (childrenFeatures == null) {
			super.getChildrenFeatures(object);
			childrenFeatures.add(LtsPackage.Literals.LTS__STATES);
			childrenFeatures.add(LtsPackage.Literals.LTS__ALPHABET);
		}
		return childrenFeatures;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EStructuralFeature getChildFeature(Object object, Object child) {
		// Check the type of the specified child object and return the proper feature to use for
		// adding (see {@link AddCommand}) it as a child.

		return super.getChildFeature(object, child);
	}

	/**
	 * This returns LTS.gif.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object getImage(Object object) {
		return overlayImage(object, getResourceLocator().getImage("full/obj16/LTS"));
	}

	/**
	 * This returns the label text for the adapted class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String getText(Object object) {
		String label = ((LTS)object).getName();
		return label == null || label.length() == 0 ?
			getString("_UI_LTS_type") :
			getString("_UI_LTS_type") + " " + label;
	}
	

	/**
	 * This handles model notifications by calling {@link #updateChildren} to update any cached
	 * children and by creating a viewer notification, which it passes to {@link #fireNotifyChanged}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void notifyChanged(Notification notification) {
		updateChildren(notification);

		switch (notification.getFeatureID(LTS.class)) {
			case LtsPackage.LTS__STATES:
			case LtsPackage.LTS__ALPHABET:
				fireNotifyChanged(new ViewerNotification(notification, notification.getNotifier(), true, false));
				return;
		}
		super.notifyChanged(notification);
	}

	/**
	 * This adds {@link org.eclipse.emf.edit.command.CommandParameter}s describing the children
	 * that can be created under this object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected void collectNewChildDescriptors(Collection<Object> newChildDescriptors, Object object) {
		super.collectNewChildDescriptors(newChildDescriptors, object);

		newChildDescriptors.add
			(createChildParameter
				(LtsPackage.Literals.LTS__STATES,
				 LtsFactory.eINSTANCE.createState()));

		newChildDescriptors.add
			(createChildParameter
				(LtsPackage.Literals.LTS__ALPHABET,
				 LtsFactory.eINSTANCE.createAlphabet()));
	}

}
