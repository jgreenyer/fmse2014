package de.luh.se.fmse.lts.ui.popup.actions;

import org.eclipse.emf.edit.command.ChangeCommand;
import org.eclipse.emf.edit.domain.IEditingDomainProvider;
import org.eclipse.jface.action.IAction;
import org.eclipse.jface.dialogs.MessageDialog;
import org.eclipse.jface.viewers.ISelection;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.ui.IActionDelegate;
import org.eclipse.ui.IEditorPart;
import org.eclipse.ui.IObjectActionDelegate;
import org.eclipse.ui.IWorkbenchPart;
import org.eclipse.ui.PlatformUI;

import de.luh.se.fmse.lts.ComposedLTS;

public class ComposeWithoutUnreachableStatesAction implements IObjectActionDelegate {

	private Shell shell;
	
	/**
	 * Constructor for Action1.
	 */
	public ComposeWithoutUnreachableStatesAction() {
		super();
	}

	/**
	 * @see IObjectActionDelegate#setActivePart(IAction, IWorkbenchPart)
	 */
	public void setActivePart(IAction action, IWorkbenchPart targetPart) {
		shell = targetPart.getSite().getShell();
	}

	/**
	 * @see IActionDelegate#run(IAction)
	 */
	public void run(IAction action) {
		
		IStructuredSelection structuredSelection = (IStructuredSelection) PlatformUI
				.getWorkbench().getActiveWorkbenchWindow()
				.getSelectionService().getSelection();
		
		if (structuredSelection.getFirstElement() instanceof ComposedLTS){

			ComposedLTS composedLTS = (ComposedLTS)structuredSelection.getFirstElement();

			// execute the change via a ChangeCommand to get undo/redo support in the editor.
			IEditorPart editorPart = PlatformUI.getWorkbench().getActiveWorkbenchWindow().getActivePage().getActiveEditor();
			if (editorPart instanceof IEditingDomainProvider){
				((IEditingDomainProvider)editorPart).getEditingDomain().getCommandStack().execute(new ChangeCommand(composedLTS.getLtsnetwork()) {
					@Override
					protected void doExecute() {
						composedLTS.compute(false);
					}
				});
			}				
				
		}
		
		MessageDialog.openInformation(
			shell,
			"LTS Ui",
			"Compose was executed.");
	}

	/**
	 * @see IActionDelegate#selectionChanged(IAction, ISelection)
	 */
	public void selectionChanged(IAction action, ISelection selection) {
	}

}
