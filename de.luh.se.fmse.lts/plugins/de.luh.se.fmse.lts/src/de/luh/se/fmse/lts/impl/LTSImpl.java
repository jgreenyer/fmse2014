/**
 */
package de.luh.se.fmse.lts.impl;

import de.luh.se.fmse.lts.Alphabet;
import de.luh.se.fmse.lts.LTS;
import de.luh.se.fmse.lts.LTSNetwork;
import de.luh.se.fmse.lts.LtsPackage;
import de.luh.se.fmse.lts.State;

import java.util.Collection;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;

import org.eclipse.emf.ecore.util.EObjectContainmentWithInverseEList;
import org.eclipse.emf.ecore.util.EcoreUtil;
import org.eclipse.emf.ecore.util.InternalEList;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>LTS</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * <ul>
 *   <li>{@link de.luh.se.fmse.lts.impl.LTSImpl#getLtsnetwork <em>Ltsnetwork</em>}</li>
 *   <li>{@link de.luh.se.fmse.lts.impl.LTSImpl#getStates <em>States</em>}</li>
 *   <li>{@link de.luh.se.fmse.lts.impl.LTSImpl#getAlphabet <em>Alphabet</em>}</li>
 *   <li>{@link de.luh.se.fmse.lts.impl.LTSImpl#getStartState <em>Start State</em>}</li>
 * </ul>
 * </p>
 *
 * @generated
 */
public class LTSImpl extends NamedElementImpl implements LTS {
	/**
	 * The cached value of the '{@link #getStates() <em>States</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getStates()
	 * @generated
	 * @ordered
	 */
	protected EList<State> states;

	/**
	 * The cached value of the '{@link #getAlphabet() <em>Alphabet</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getAlphabet()
	 * @generated
	 * @ordered
	 */
	protected Alphabet alphabet;

	/**
	 * The cached value of the '{@link #getStartState() <em>Start State</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getStartState()
	 * @generated
	 * @ordered
	 */
	protected State startState;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected LTSImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return LtsPackage.Literals.LTS;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public LTSNetwork getLtsnetwork() {
		if (eContainerFeatureID() != LtsPackage.LTS__LTSNETWORK) return null;
		return (LTSNetwork)eInternalContainer();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetLtsnetwork(LTSNetwork newLtsnetwork, NotificationChain msgs) {
		msgs = eBasicSetContainer((InternalEObject)newLtsnetwork, LtsPackage.LTS__LTSNETWORK, msgs);
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setLtsnetwork(LTSNetwork newLtsnetwork) {
		if (newLtsnetwork != eInternalContainer() || (eContainerFeatureID() != LtsPackage.LTS__LTSNETWORK && newLtsnetwork != null)) {
			if (EcoreUtil.isAncestor(this, newLtsnetwork))
				throw new IllegalArgumentException("Recursive containment not allowed for " + toString());
			NotificationChain msgs = null;
			if (eInternalContainer() != null)
				msgs = eBasicRemoveFromContainer(msgs);
			if (newLtsnetwork != null)
				msgs = ((InternalEObject)newLtsnetwork).eInverseAdd(this, LtsPackage.LTS_NETWORK__LTSS, LTSNetwork.class, msgs);
			msgs = basicSetLtsnetwork(newLtsnetwork, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, LtsPackage.LTS__LTSNETWORK, newLtsnetwork, newLtsnetwork));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<State> getStates() {
		if (states == null) {
			states = new EObjectContainmentWithInverseEList<State>(State.class, this, LtsPackage.LTS__STATES, LtsPackage.STATE__CONTAINING_LTS);
		}
		return states;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Alphabet getAlphabet() {
		return alphabet;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetAlphabet(Alphabet newAlphabet, NotificationChain msgs) {
		Alphabet oldAlphabet = alphabet;
		alphabet = newAlphabet;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, LtsPackage.LTS__ALPHABET, oldAlphabet, newAlphabet);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setAlphabet(Alphabet newAlphabet) {
		if (newAlphabet != alphabet) {
			NotificationChain msgs = null;
			if (alphabet != null)
				msgs = ((InternalEObject)alphabet).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - LtsPackage.LTS__ALPHABET, null, msgs);
			if (newAlphabet != null)
				msgs = ((InternalEObject)newAlphabet).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - LtsPackage.LTS__ALPHABET, null, msgs);
			msgs = basicSetAlphabet(newAlphabet, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, LtsPackage.LTS__ALPHABET, newAlphabet, newAlphabet));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public State getStartState() {
		if (startState != null && startState.eIsProxy()) {
			InternalEObject oldStartState = (InternalEObject)startState;
			startState = (State)eResolveProxy(oldStartState);
			if (startState != oldStartState) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, LtsPackage.LTS__START_STATE, oldStartState, startState));
			}
		}
		return startState;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public State basicGetStartState() {
		return startState;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setStartState(State newStartState) {
		State oldStartState = startState;
		startState = newStartState;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, LtsPackage.LTS__START_STATE, oldStartState, startState));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public NotificationChain eInverseAdd(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case LtsPackage.LTS__LTSNETWORK:
				if (eInternalContainer() != null)
					msgs = eBasicRemoveFromContainer(msgs);
				return basicSetLtsnetwork((LTSNetwork)otherEnd, msgs);
			case LtsPackage.LTS__STATES:
				return ((InternalEList<InternalEObject>)(InternalEList<?>)getStates()).basicAdd(otherEnd, msgs);
		}
		return super.eInverseAdd(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case LtsPackage.LTS__LTSNETWORK:
				return basicSetLtsnetwork(null, msgs);
			case LtsPackage.LTS__STATES:
				return ((InternalEList<?>)getStates()).basicRemove(otherEnd, msgs);
			case LtsPackage.LTS__ALPHABET:
				return basicSetAlphabet(null, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eBasicRemoveFromContainerFeature(NotificationChain msgs) {
		switch (eContainerFeatureID()) {
			case LtsPackage.LTS__LTSNETWORK:
				return eInternalContainer().eInverseRemove(this, LtsPackage.LTS_NETWORK__LTSS, LTSNetwork.class, msgs);
		}
		return super.eBasicRemoveFromContainerFeature(msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case LtsPackage.LTS__LTSNETWORK:
				return getLtsnetwork();
			case LtsPackage.LTS__STATES:
				return getStates();
			case LtsPackage.LTS__ALPHABET:
				return getAlphabet();
			case LtsPackage.LTS__START_STATE:
				if (resolve) return getStartState();
				return basicGetStartState();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case LtsPackage.LTS__LTSNETWORK:
				setLtsnetwork((LTSNetwork)newValue);
				return;
			case LtsPackage.LTS__STATES:
				getStates().clear();
				getStates().addAll((Collection<? extends State>)newValue);
				return;
			case LtsPackage.LTS__ALPHABET:
				setAlphabet((Alphabet)newValue);
				return;
			case LtsPackage.LTS__START_STATE:
				setStartState((State)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case LtsPackage.LTS__LTSNETWORK:
				setLtsnetwork((LTSNetwork)null);
				return;
			case LtsPackage.LTS__STATES:
				getStates().clear();
				return;
			case LtsPackage.LTS__ALPHABET:
				setAlphabet((Alphabet)null);
				return;
			case LtsPackage.LTS__START_STATE:
				setStartState((State)null);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case LtsPackage.LTS__LTSNETWORK:
				return getLtsnetwork() != null;
			case LtsPackage.LTS__STATES:
				return states != null && !states.isEmpty();
			case LtsPackage.LTS__ALPHABET:
				return alphabet != null;
			case LtsPackage.LTS__START_STATE:
				return startState != null;
		}
		return super.eIsSet(featureID);
	}

} //LTSImpl
