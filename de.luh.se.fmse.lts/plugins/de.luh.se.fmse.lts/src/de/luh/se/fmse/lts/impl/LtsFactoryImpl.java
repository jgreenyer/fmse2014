/**
 */
package de.luh.se.fmse.lts.impl;

import de.luh.se.fmse.lts.*;

import java.util.Map;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EPackage;

import org.eclipse.emf.ecore.impl.EFactoryImpl;

import org.eclipse.emf.ecore.plugin.EcorePlugin;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model <b>Factory</b>.
 * <!-- end-user-doc -->
 * @generated
 */
public class LtsFactoryImpl extends EFactoryImpl implements LtsFactory {
	/**
	 * Creates the default factory implementation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static LtsFactory init() {
		try {
			LtsFactory theLtsFactory = (LtsFactory)EPackage.Registry.INSTANCE.getEFactory(LtsPackage.eNS_URI);
			if (theLtsFactory != null) {
				return theLtsFactory;
			}
		}
		catch (Exception exception) {
			EcorePlugin.INSTANCE.log(exception);
		}
		return new LtsFactoryImpl();
	}

	/**
	 * Creates an instance of the factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public LtsFactoryImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EObject create(EClass eClass) {
		switch (eClass.getClassifierID()) {
			case LtsPackage.LTS: return createLTS();
			case LtsPackage.COMPOSED_LTS: return createComposedLTS();
			case LtsPackage.LTS_NETWORK: return createLTSNetwork();
			case LtsPackage.STATE: return createState();
			case LtsPackage.TRANSITION: return createTransition();
			case LtsPackage.NAMED_ELEMENT: return createNamedElement();
			case LtsPackage.ALPHABET: return createAlphabet();
			case LtsPackage.EVENT: return createEvent();
			case LtsPackage.EVENT_DECLARATIONS: return createEventDeclarations();
			case LtsPackage.STATE_LIST_TO_COMPOSED_STATE_MAP_ENTRY: return (EObject)createStateListToComposedStateMapEntry();
			default:
				throw new IllegalArgumentException("The class '" + eClass.getName() + "' is not a valid classifier");
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public LTS createLTS() {
		LTSImpl lts = new LTSImpl();
		return lts;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ComposedLTS createComposedLTS() {
		ComposedLTSImpl composedLTS = new ComposedLTSImpl();
		return composedLTS;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public LTSNetwork createLTSNetwork() {
		LTSNetworkImpl ltsNetwork = new LTSNetworkImpl();
		return ltsNetwork;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public State createState() {
		StateImpl state = new StateImpl();
		return state;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Transition createTransition() {
		TransitionImpl transition = new TransitionImpl();
		return transition;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NamedElement createNamedElement() {
		NamedElementImpl namedElement = new NamedElementImpl();
		return namedElement;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Alphabet createAlphabet() {
		AlphabetImpl alphabet = new AlphabetImpl();
		return alphabet;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Event createEvent() {
		EventImpl event = new EventImpl();
		return event;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EventDeclarations createEventDeclarations() {
		EventDeclarationsImpl eventDeclarations = new EventDeclarationsImpl();
		return eventDeclarations;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Map.Entry<EList<State>, State> createStateListToComposedStateMapEntry() {
		StateListToComposedStateMapEntryImpl stateListToComposedStateMapEntry = new StateListToComposedStateMapEntryImpl();
		return stateListToComposedStateMapEntry;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public LtsPackage getLtsPackage() {
		return (LtsPackage)getEPackage();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @deprecated
	 * @generated
	 */
	@Deprecated
	public static LtsPackage getPackage() {
		return LtsPackage.eINSTANCE;
	}

} //LtsFactoryImpl
