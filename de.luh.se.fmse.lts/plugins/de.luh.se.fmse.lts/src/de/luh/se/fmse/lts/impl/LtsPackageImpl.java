/**
 */
package de.luh.se.fmse.lts.impl;

import de.luh.se.fmse.lts.Alphabet;
import de.luh.se.fmse.lts.ComposedLTS;
import de.luh.se.fmse.lts.Event;
import de.luh.se.fmse.lts.EventDeclarations;
import de.luh.se.fmse.lts.LTSNetwork;
import de.luh.se.fmse.lts.LtsFactory;
import de.luh.se.fmse.lts.LtsPackage;
import de.luh.se.fmse.lts.NamedElement;
import de.luh.se.fmse.lts.State;
import de.luh.se.fmse.lts.Transition;
import java.util.Map;
import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EOperation;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.EReference;
import org.eclipse.emf.ecore.impl.EPackageImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model <b>Package</b>.
 * <!-- end-user-doc -->
 * @generated
 */
public class LtsPackageImpl extends EPackageImpl implements LtsPackage {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass ltsEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass composedLTSEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass ltsNetworkEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass stateEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass transitionEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass namedElementEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass alphabetEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass eventEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass eventDeclarationsEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass stateListToComposedStateMapEntryEClass = null;

	/**
	 * Creates an instance of the model <b>Package</b>, registered with
	 * {@link org.eclipse.emf.ecore.EPackage.Registry EPackage.Registry} by the package
	 * package URI value.
	 * <p>Note: the correct way to create the package is via the static
	 * factory method {@link #init init()}, which also performs
	 * initialization of the package, or returns the registered package,
	 * if one already exists.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.emf.ecore.EPackage.Registry
	 * @see de.luh.se.fmse.lts.LtsPackage#eNS_URI
	 * @see #init()
	 * @generated
	 */
	private LtsPackageImpl() {
		super(eNS_URI, LtsFactory.eINSTANCE);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private static boolean isInited = false;

	/**
	 * Creates, registers, and initializes the <b>Package</b> for this model, and for any others upon which it depends.
	 * 
	 * <p>This method is used to initialize {@link LtsPackage#eINSTANCE} when that field is accessed.
	 * Clients should not invoke it directly. Instead, they should simply access that field to obtain the package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #eNS_URI
	 * @see #createPackageContents()
	 * @see #initializePackageContents()
	 * @generated
	 */
	public static LtsPackage init() {
		if (isInited) return (LtsPackage)EPackage.Registry.INSTANCE.getEPackage(LtsPackage.eNS_URI);

		// Obtain or create and register package
		LtsPackageImpl theLtsPackage = (LtsPackageImpl)(EPackage.Registry.INSTANCE.get(eNS_URI) instanceof LtsPackageImpl ? EPackage.Registry.INSTANCE.get(eNS_URI) : new LtsPackageImpl());

		isInited = true;

		// Create package meta-data objects
		theLtsPackage.createPackageContents();

		// Initialize created meta-data
		theLtsPackage.initializePackageContents();

		// Mark meta-data to indicate it can't be changed
		theLtsPackage.freeze();

  
		// Update the registry and return the package
		EPackage.Registry.INSTANCE.put(LtsPackage.eNS_URI, theLtsPackage);
		return theLtsPackage;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getLTS() {
		return ltsEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getLTS_Ltsnetwork() {
		return (EReference)ltsEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getLTS_States() {
		return (EReference)ltsEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getLTS_Alphabet() {
		return (EReference)ltsEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getLTS_StartState() {
		return (EReference)ltsEClass.getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getComposedLTS() {
		return composedLTSEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getComposedLTS_ConstituentLTSs() {
		return (EReference)composedLTSEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getComposedLTS_StateListToComposedStateMap() {
		return (EReference)composedLTSEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getComposedLTS__Compute() {
		return composedLTSEClass.getEOperations().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getComposedLTS__Compute__boolean() {
		return composedLTSEClass.getEOperations().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getComposedLTS__RegisterComposedState__State_State_State() {
		return composedLTSEClass.getEOperations().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getComposedLTS__LookupOrCreateComposedState__State_State() {
		return composedLTSEClass.getEOperations().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getLTSNetwork() {
		return ltsNetworkEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getLTSNetwork_Ltss() {
		return (EReference)ltsNetworkEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getLTSNetwork_EventDeclarations() {
		return (EReference)ltsNetworkEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getState() {
		return stateEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getState_OutgoingTransitions() {
		return (EReference)stateEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getState_IncomingTransitions() {
		return (EReference)stateEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getState_ContainingLTS() {
		return (EReference)stateEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getState__CreateOutgoingTransitions__EList_State_State() {
		return stateEClass.getEOperations().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getTransition() {
		return transitionEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getTransition_SourceState() {
		return (EReference)transitionEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getTransition_TargetState() {
		return (EReference)transitionEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getTransition_Label() {
		return (EReference)transitionEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getNamedElement() {
		return namedElementEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getNamedElement_Name() {
		return (EAttribute)namedElementEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getAlphabet() {
		return alphabetEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getAlphabet_Events() {
		return (EReference)alphabetEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getEvent() {
		return eventEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getEventDeclarations() {
		return eventDeclarationsEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getEventDeclarations_DeclaredEvents() {
		return (EReference)eventDeclarationsEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getStateListToComposedStateMapEntry() {
		return stateListToComposedStateMapEntryEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getStateListToComposedStateMapEntry_Key() {
		return (EReference)stateListToComposedStateMapEntryEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getStateListToComposedStateMapEntry_Value() {
		return (EReference)stateListToComposedStateMapEntryEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public LtsFactory getLtsFactory() {
		return (LtsFactory)getEFactoryInstance();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private boolean isCreated = false;

	/**
	 * Creates the meta-model objects for the package.  This method is
	 * guarded to have no affect on any invocation but its first.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void createPackageContents() {
		if (isCreated) return;
		isCreated = true;

		// Create classes and their features
		ltsEClass = createEClass(LTS);
		createEReference(ltsEClass, LTS__LTSNETWORK);
		createEReference(ltsEClass, LTS__STATES);
		createEReference(ltsEClass, LTS__ALPHABET);
		createEReference(ltsEClass, LTS__START_STATE);

		composedLTSEClass = createEClass(COMPOSED_LTS);
		createEReference(composedLTSEClass, COMPOSED_LTS__CONSTITUENT_LT_SS);
		createEReference(composedLTSEClass, COMPOSED_LTS__STATE_LIST_TO_COMPOSED_STATE_MAP);
		createEOperation(composedLTSEClass, COMPOSED_LTS___COMPUTE);
		createEOperation(composedLTSEClass, COMPOSED_LTS___COMPUTE__BOOLEAN);
		createEOperation(composedLTSEClass, COMPOSED_LTS___REGISTER_COMPOSED_STATE__STATE_STATE_STATE);
		createEOperation(composedLTSEClass, COMPOSED_LTS___LOOKUP_OR_CREATE_COMPOSED_STATE__STATE_STATE);

		ltsNetworkEClass = createEClass(LTS_NETWORK);
		createEReference(ltsNetworkEClass, LTS_NETWORK__LTSS);
		createEReference(ltsNetworkEClass, LTS_NETWORK__EVENT_DECLARATIONS);

		stateEClass = createEClass(STATE);
		createEReference(stateEClass, STATE__OUTGOING_TRANSITIONS);
		createEReference(stateEClass, STATE__INCOMING_TRANSITIONS);
		createEReference(stateEClass, STATE__CONTAINING_LTS);
		createEOperation(stateEClass, STATE___CREATE_OUTGOING_TRANSITIONS__ELIST_STATE_STATE);

		transitionEClass = createEClass(TRANSITION);
		createEReference(transitionEClass, TRANSITION__SOURCE_STATE);
		createEReference(transitionEClass, TRANSITION__TARGET_STATE);
		createEReference(transitionEClass, TRANSITION__LABEL);

		namedElementEClass = createEClass(NAMED_ELEMENT);
		createEAttribute(namedElementEClass, NAMED_ELEMENT__NAME);

		alphabetEClass = createEClass(ALPHABET);
		createEReference(alphabetEClass, ALPHABET__EVENTS);

		eventEClass = createEClass(EVENT);

		eventDeclarationsEClass = createEClass(EVENT_DECLARATIONS);
		createEReference(eventDeclarationsEClass, EVENT_DECLARATIONS__DECLARED_EVENTS);

		stateListToComposedStateMapEntryEClass = createEClass(STATE_LIST_TO_COMPOSED_STATE_MAP_ENTRY);
		createEReference(stateListToComposedStateMapEntryEClass, STATE_LIST_TO_COMPOSED_STATE_MAP_ENTRY__KEY);
		createEReference(stateListToComposedStateMapEntryEClass, STATE_LIST_TO_COMPOSED_STATE_MAP_ENTRY__VALUE);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private boolean isInitialized = false;

	/**
	 * Complete the initialization of the package and its meta-model.  This
	 * method is guarded to have no affect on any invocation but its first.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void initializePackageContents() {
		if (isInitialized) return;
		isInitialized = true;

		// Initialize package
		setName(eNAME);
		setNsPrefix(eNS_PREFIX);
		setNsURI(eNS_URI);

		// Create type parameters

		// Set bounds for type parameters

		// Add supertypes to classes
		ltsEClass.getESuperTypes().add(this.getNamedElement());
		composedLTSEClass.getESuperTypes().add(this.getLTS());
		ltsNetworkEClass.getESuperTypes().add(this.getNamedElement());
		stateEClass.getESuperTypes().add(this.getNamedElement());
		eventEClass.getESuperTypes().add(this.getNamedElement());

		// Initialize classes, features, and operations; add parameters
		initEClass(ltsEClass, de.luh.se.fmse.lts.LTS.class, "LTS", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getLTS_Ltsnetwork(), this.getLTSNetwork(), this.getLTSNetwork_Ltss(), "ltsnetwork", null, 0, 1, de.luh.se.fmse.lts.LTS.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getLTS_States(), this.getState(), this.getState_ContainingLTS(), "states", null, 0, -1, de.luh.se.fmse.lts.LTS.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getLTS_Alphabet(), this.getAlphabet(), null, "alphabet", null, 0, 1, de.luh.se.fmse.lts.LTS.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getLTS_StartState(), this.getState(), null, "startState", null, 0, 1, de.luh.se.fmse.lts.LTS.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(composedLTSEClass, ComposedLTS.class, "ComposedLTS", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getComposedLTS_ConstituentLTSs(), this.getLTS(), null, "constituentLTSs", null, 0, -1, ComposedLTS.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getComposedLTS_StateListToComposedStateMap(), this.getStateListToComposedStateMapEntry(), null, "stateListToComposedStateMap", null, 0, -1, ComposedLTS.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEOperation(getComposedLTS__Compute(), null, "compute", 0, 1, IS_UNIQUE, IS_ORDERED);

		EOperation op = initEOperation(getComposedLTS__Compute__boolean(), null, "compute", 0, 1, IS_UNIQUE, IS_ORDERED);
		addEParameter(op, ecorePackage.getEBoolean(), "computeUnreachableStates", 0, 1, IS_UNIQUE, IS_ORDERED);

		op = initEOperation(getComposedLTS__RegisterComposedState__State_State_State(), this.getStateListToComposedStateMapEntry(), "registerComposedState", 0, 1, IS_UNIQUE, IS_ORDERED);
		addEParameter(op, this.getState(), "composedState", 0, 1, IS_UNIQUE, IS_ORDERED);
		addEParameter(op, this.getState(), "constituentState1", 0, 1, IS_UNIQUE, IS_ORDERED);
		addEParameter(op, this.getState(), "constituentState2", 0, 1, IS_UNIQUE, IS_ORDERED);

		op = initEOperation(getComposedLTS__LookupOrCreateComposedState__State_State(), this.getStateListToComposedStateMapEntry(), "lookupOrCreateComposedState", 0, 1, IS_UNIQUE, IS_ORDERED);
		addEParameter(op, this.getState(), "constituentState1", 0, 1, IS_UNIQUE, IS_ORDERED);
		addEParameter(op, this.getState(), "constituentState2", 0, 1, IS_UNIQUE, IS_ORDERED);

		initEClass(ltsNetworkEClass, LTSNetwork.class, "LTSNetwork", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getLTSNetwork_Ltss(), this.getLTS(), this.getLTS_Ltsnetwork(), "ltss", null, 0, -1, LTSNetwork.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getLTSNetwork_EventDeclarations(), this.getEventDeclarations(), null, "eventDeclarations", null, 1, 1, LTSNetwork.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(stateEClass, State.class, "State", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getState_OutgoingTransitions(), this.getTransition(), this.getTransition_SourceState(), "outgoingTransitions", null, 0, -1, State.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getState_IncomingTransitions(), this.getTransition(), this.getTransition_TargetState(), "incomingTransitions", null, 0, -1, State.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getState_ContainingLTS(), this.getLTS(), this.getLTS_States(), "containingLTS", null, 1, 1, State.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		op = initEOperation(getState__CreateOutgoingTransitions__EList_State_State(), this.getStateListToComposedStateMapEntry(), "createOutgoingTransitions", 0, -1, IS_UNIQUE, IS_ORDERED);
		addEParameter(op, this.getEvent(), "sharedEvents", 0, -1, IS_UNIQUE, IS_ORDERED);
		addEParameter(op, this.getState(), "constituentState1", 0, 1, IS_UNIQUE, IS_ORDERED);
		addEParameter(op, this.getState(), "constituentState2", 0, 1, IS_UNIQUE, IS_ORDERED);

		initEClass(transitionEClass, Transition.class, "Transition", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getTransition_SourceState(), this.getState(), this.getState_OutgoingTransitions(), "sourceState", null, 1, 1, Transition.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getTransition_TargetState(), this.getState(), this.getState_IncomingTransitions(), "targetState", null, 1, 1, Transition.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getTransition_Label(), this.getEvent(), null, "label", null, 1, 1, Transition.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(namedElementEClass, NamedElement.class, "NamedElement", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getNamedElement_Name(), ecorePackage.getEString(), "name", null, 0, 1, NamedElement.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(alphabetEClass, Alphabet.class, "Alphabet", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getAlphabet_Events(), this.getEvent(), null, "events", null, 0, -1, Alphabet.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(eventEClass, Event.class, "Event", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

		initEClass(eventDeclarationsEClass, EventDeclarations.class, "EventDeclarations", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getEventDeclarations_DeclaredEvents(), this.getEvent(), null, "declaredEvents", null, 0, -1, EventDeclarations.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(stateListToComposedStateMapEntryEClass, Map.Entry.class, "StateListToComposedStateMapEntry", !IS_ABSTRACT, !IS_INTERFACE, !IS_GENERATED_INSTANCE_CLASS);
		initEReference(getStateListToComposedStateMapEntry_Key(), this.getState(), null, "key", null, 0, -1, Map.Entry.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getStateListToComposedStateMapEntry_Value(), this.getState(), null, "value", null, 0, 1, Map.Entry.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		// Create resource
		createResource(eNS_URI);
	}

} //LtsPackageImpl
