/**
 */
package de.luh.se.fmse.lts.impl;

import de.luh.se.fmse.lts.ComposedLTS;
import de.luh.se.fmse.lts.Event;
import de.luh.se.fmse.lts.LTS;
import de.luh.se.fmse.lts.LtsPackage;
import de.luh.se.fmse.lts.State;
import de.luh.se.fmse.lts.Transition;

import java.lang.reflect.InvocationTargetException;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;
import org.eclipse.emf.common.util.BasicEList;
import org.eclipse.emf.common.util.BasicEMap;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.common.util.EMap;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;
import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.util.EObjectContainmentWithInverseEList;
import org.eclipse.emf.ecore.util.EObjectWithInverseResolvingEList;
import org.eclipse.emf.ecore.util.EcoreUtil;
import org.eclipse.emf.ecore.util.InternalEList;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>State</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * <ul>
 *   <li>{@link de.luh.se.fmse.lts.impl.StateImpl#getOutgoingTransitions <em>Outgoing Transitions</em>}</li>
 *   <li>{@link de.luh.se.fmse.lts.impl.StateImpl#getIncomingTransitions <em>Incoming Transitions</em>}</li>
 *   <li>{@link de.luh.se.fmse.lts.impl.StateImpl#getContainingLTS <em>Containing LTS</em>}</li>
 * </ul>
 * </p>
 *
 * @generated
 */
public class StateImpl extends NamedElementImpl implements State {
	/**
	 * The cached value of the '{@link #getOutgoingTransitions() <em>Outgoing Transitions</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getOutgoingTransitions()
	 * @generated
	 * @ordered
	 */
	protected EList<Transition> outgoingTransitions;

	/**
	 * The cached value of the '{@link #getIncomingTransitions() <em>Incoming Transitions</em>}' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getIncomingTransitions()
	 * @generated
	 * @ordered
	 */
	protected EList<Transition> incomingTransitions;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected StateImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return LtsPackage.Literals.STATE;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<Transition> getOutgoingTransitions() {
		if (outgoingTransitions == null) {
			outgoingTransitions = new EObjectContainmentWithInverseEList<Transition>(Transition.class, this, LtsPackage.STATE__OUTGOING_TRANSITIONS, LtsPackage.TRANSITION__SOURCE_STATE);
		}
		return outgoingTransitions;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<Transition> getIncomingTransitions() {
		if (incomingTransitions == null) {
			incomingTransitions = new EObjectWithInverseResolvingEList<Transition>(Transition.class, this, LtsPackage.STATE__INCOMING_TRANSITIONS, LtsPackage.TRANSITION__TARGET_STATE);
		}
		return incomingTransitions;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public LTS getContainingLTS() {
		if (eContainerFeatureID() != LtsPackage.STATE__CONTAINING_LTS) return null;
		return (LTS)eInternalContainer();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetContainingLTS(LTS newContainingLTS, NotificationChain msgs) {
		msgs = eBasicSetContainer((InternalEObject)newContainingLTS, LtsPackage.STATE__CONTAINING_LTS, msgs);
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setContainingLTS(LTS newContainingLTS) {
		if (newContainingLTS != eInternalContainer() || (eContainerFeatureID() != LtsPackage.STATE__CONTAINING_LTS && newContainingLTS != null)) {
			if (EcoreUtil.isAncestor(this, newContainingLTS))
				throw new IllegalArgumentException("Recursive containment not allowed for " + toString());
			NotificationChain msgs = null;
			if (eInternalContainer() != null)
				msgs = eBasicRemoveFromContainer(msgs);
			if (newContainingLTS != null)
				msgs = ((InternalEObject)newContainingLTS).eInverseAdd(this, LtsPackage.LTS__STATES, LTS.class, msgs);
			msgs = basicSetContainingLTS(newContainingLTS, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, LtsPackage.STATE__CONTAINING_LTS, newContainingLTS, newContainingLTS));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public EMap<EList<State>, State> createOutgoingTransitions(EList<Event> sharedEvents, State constituentState1, State constituentState2) {

		EMap<EList<State>, State> targetStates = new BasicEMap<EList<State>, State>();
		
		EList<Transition> transitionsIndependentInLTS1 = new BasicEList<Transition>();
		EList<Transition> transitionsIndependentInLTS2 = new BasicEList<Transition>();
		HashMap<Event, Transition> sharedEventToTransitionsInLTS1 = new HashMap<Event, Transition>();
		HashMap<Event, Transition> sharedEventToTransitionsInLTS2 = new HashMap<Event, Transition>();
		
		
		// iterate over outgoing transitions of state1 and state2.
		// If a transition is labeled with a shared event, and thus must be synchronized,
		// then store this event in a Event->Transition map for faster lookup later.
		// Otherwise, if the transition is labeled with a non-shared event, then put it into
		// a list, for faster lookup later.
		for (Transition transition : constituentState1.getOutgoingTransitions()) {
			if(sharedEvents.contains(transition.getLabel())){
				sharedEventToTransitionsInLTS1.put(transition.getLabel(), transition);
			}else{
				transitionsIndependentInLTS1.add(transition);
			}
				
		}
		for (Transition transition : constituentState2.getOutgoingTransitions()) {
			if(sharedEvents.contains(transition.getLabel())){
				sharedEventToTransitionsInLTS2.put(transition.getLabel(), transition);
			}else{
				transitionsIndependentInLTS2.add(transition);
			}
				
		}
		
		// create transitions in composed LTS that are taken independently in LTS1
		for (Transition transition : transitionsIndependentInLTS1) {
			Transition transitionInComposedLTS = LtsPackage.eINSTANCE.getLtsFactory().createTransition();
			transitionInComposedLTS.setLabel(transition.getLabel());
			Map.Entry<EList<State>, State> targetStateMapEntry = ((ComposedLTS)getContainingLTS()).lookupOrCreateComposedState(transition.getTargetState(), constituentState2);
			getOutgoingTransitions().add(transitionInComposedLTS);
			transitionInComposedLTS.setTargetState(targetStateMapEntry.getValue());
			targetStates.add(targetStateMapEntry);
		}
		// create transitions in composed LTS that are taken independently in LTS2
		for (Transition transition : transitionsIndependentInLTS2) {
			Transition transitionInComposedLTS = LtsPackage.eINSTANCE.getLtsFactory().createTransition();
			transitionInComposedLTS.setLabel(transition.getLabel());
			Map.Entry<EList<State>, State> targetStateMapEntry = ((ComposedLTS)getContainingLTS()).lookupOrCreateComposedState(constituentState1, transition.getTargetState());
			getOutgoingTransitions().add(transitionInComposedLTS);
			transitionInComposedLTS.setTargetState(targetStateMapEntry.getValue());
			targetStates.add(targetStateMapEntry);
		}
		
		// compute the events for which both states have an outgoing transition
		Set<Event> sharedEventsLabelingOutgoingTransitioninBothLTSs = new HashSet<Event>(sharedEventToTransitionsInLTS1.keySet()); 
		sharedEventsLabelingOutgoingTransitioninBothLTSs.retainAll(sharedEventToTransitionsInLTS2.keySet());
		
		// for each of these events, create a corresponding outgoing transition.
		for (Event event : sharedEventsLabelingOutgoingTransitioninBothLTSs) {
			Transition transitionInComposedLTS = LtsPackage.eINSTANCE.getLtsFactory().createTransition();
			transitionInComposedLTS.setLabel(event);
			Map.Entry<EList<State>, State> targetStateMapEntry = ((ComposedLTS)getContainingLTS()).lookupOrCreateComposedState(
					sharedEventToTransitionsInLTS1.get(event).getTargetState(), 
					sharedEventToTransitionsInLTS2.get(event).getTargetState());
			getOutgoingTransitions().add(transitionInComposedLTS);
			transitionInComposedLTS.setTargetState(targetStateMapEntry.getValue());
			targetStates.add(targetStateMapEntry);
		}

		return targetStates;
		
	}
	
	

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public NotificationChain eInverseAdd(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case LtsPackage.STATE__OUTGOING_TRANSITIONS:
				return ((InternalEList<InternalEObject>)(InternalEList<?>)getOutgoingTransitions()).basicAdd(otherEnd, msgs);
			case LtsPackage.STATE__INCOMING_TRANSITIONS:
				return ((InternalEList<InternalEObject>)(InternalEList<?>)getIncomingTransitions()).basicAdd(otherEnd, msgs);
			case LtsPackage.STATE__CONTAINING_LTS:
				if (eInternalContainer() != null)
					msgs = eBasicRemoveFromContainer(msgs);
				return basicSetContainingLTS((LTS)otherEnd, msgs);
		}
		return super.eInverseAdd(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case LtsPackage.STATE__OUTGOING_TRANSITIONS:
				return ((InternalEList<?>)getOutgoingTransitions()).basicRemove(otherEnd, msgs);
			case LtsPackage.STATE__INCOMING_TRANSITIONS:
				return ((InternalEList<?>)getIncomingTransitions()).basicRemove(otherEnd, msgs);
			case LtsPackage.STATE__CONTAINING_LTS:
				return basicSetContainingLTS(null, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eBasicRemoveFromContainerFeature(NotificationChain msgs) {
		switch (eContainerFeatureID()) {
			case LtsPackage.STATE__CONTAINING_LTS:
				return eInternalContainer().eInverseRemove(this, LtsPackage.LTS__STATES, LTS.class, msgs);
		}
		return super.eBasicRemoveFromContainerFeature(msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case LtsPackage.STATE__OUTGOING_TRANSITIONS:
				return getOutgoingTransitions();
			case LtsPackage.STATE__INCOMING_TRANSITIONS:
				return getIncomingTransitions();
			case LtsPackage.STATE__CONTAINING_LTS:
				return getContainingLTS();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case LtsPackage.STATE__OUTGOING_TRANSITIONS:
				getOutgoingTransitions().clear();
				getOutgoingTransitions().addAll((Collection<? extends Transition>)newValue);
				return;
			case LtsPackage.STATE__INCOMING_TRANSITIONS:
				getIncomingTransitions().clear();
				getIncomingTransitions().addAll((Collection<? extends Transition>)newValue);
				return;
			case LtsPackage.STATE__CONTAINING_LTS:
				setContainingLTS((LTS)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case LtsPackage.STATE__OUTGOING_TRANSITIONS:
				getOutgoingTransitions().clear();
				return;
			case LtsPackage.STATE__INCOMING_TRANSITIONS:
				getIncomingTransitions().clear();
				return;
			case LtsPackage.STATE__CONTAINING_LTS:
				setContainingLTS((LTS)null);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case LtsPackage.STATE__OUTGOING_TRANSITIONS:
				return outgoingTransitions != null && !outgoingTransitions.isEmpty();
			case LtsPackage.STATE__INCOMING_TRANSITIONS:
				return incomingTransitions != null && !incomingTransitions.isEmpty();
			case LtsPackage.STATE__CONTAINING_LTS:
				return getContainingLTS() != null;
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	@SuppressWarnings("unchecked")
	public Object eInvoke(int operationID, EList<?> arguments) throws InvocationTargetException {
		switch (operationID) {
			case LtsPackage.STATE___CREATE_OUTGOING_TRANSITIONS__ELIST_STATE_STATE:
				return createOutgoingTransitions((EList<Event>)arguments.get(0), (State)arguments.get(1), (State)arguments.get(2));
		}
		return super.eInvoke(operationID, arguments);
	}

} //StateImpl
