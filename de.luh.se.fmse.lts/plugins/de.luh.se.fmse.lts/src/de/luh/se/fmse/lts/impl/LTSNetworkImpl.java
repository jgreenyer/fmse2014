/**
 */
package de.luh.se.fmse.lts.impl;

import de.luh.se.fmse.lts.EventDeclarations;
import de.luh.se.fmse.lts.LTS;
import de.luh.se.fmse.lts.LTSNetwork;
import de.luh.se.fmse.lts.LtsPackage;

import java.util.Collection;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;

import org.eclipse.emf.ecore.util.EObjectContainmentWithInverseEList;
import org.eclipse.emf.ecore.util.InternalEList;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>LTS Network</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * <ul>
 *   <li>{@link de.luh.se.fmse.lts.impl.LTSNetworkImpl#getLtss <em>Ltss</em>}</li>
 *   <li>{@link de.luh.se.fmse.lts.impl.LTSNetworkImpl#getEventDeclarations <em>Event Declarations</em>}</li>
 * </ul>
 * </p>
 *
 * @generated
 */
public class LTSNetworkImpl extends NamedElementImpl implements LTSNetwork {
	/**
	 * The cached value of the '{@link #getLtss() <em>Ltss</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getLtss()
	 * @generated
	 * @ordered
	 */
	protected EList<LTS> ltss;

	/**
	 * The cached value of the '{@link #getEventDeclarations() <em>Event Declarations</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getEventDeclarations()
	 * @generated
	 * @ordered
	 */
	protected EventDeclarations eventDeclarations;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected LTSNetworkImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return LtsPackage.Literals.LTS_NETWORK;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<LTS> getLtss() {
		if (ltss == null) {
			ltss = new EObjectContainmentWithInverseEList<LTS>(LTS.class, this, LtsPackage.LTS_NETWORK__LTSS, LtsPackage.LTS__LTSNETWORK);
		}
		return ltss;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EventDeclarations getEventDeclarations() {
		return eventDeclarations;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetEventDeclarations(EventDeclarations newEventDeclarations, NotificationChain msgs) {
		EventDeclarations oldEventDeclarations = eventDeclarations;
		eventDeclarations = newEventDeclarations;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, LtsPackage.LTS_NETWORK__EVENT_DECLARATIONS, oldEventDeclarations, newEventDeclarations);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setEventDeclarations(EventDeclarations newEventDeclarations) {
		if (newEventDeclarations != eventDeclarations) {
			NotificationChain msgs = null;
			if (eventDeclarations != null)
				msgs = ((InternalEObject)eventDeclarations).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - LtsPackage.LTS_NETWORK__EVENT_DECLARATIONS, null, msgs);
			if (newEventDeclarations != null)
				msgs = ((InternalEObject)newEventDeclarations).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - LtsPackage.LTS_NETWORK__EVENT_DECLARATIONS, null, msgs);
			msgs = basicSetEventDeclarations(newEventDeclarations, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, LtsPackage.LTS_NETWORK__EVENT_DECLARATIONS, newEventDeclarations, newEventDeclarations));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public NotificationChain eInverseAdd(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case LtsPackage.LTS_NETWORK__LTSS:
				return ((InternalEList<InternalEObject>)(InternalEList<?>)getLtss()).basicAdd(otherEnd, msgs);
		}
		return super.eInverseAdd(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case LtsPackage.LTS_NETWORK__LTSS:
				return ((InternalEList<?>)getLtss()).basicRemove(otherEnd, msgs);
			case LtsPackage.LTS_NETWORK__EVENT_DECLARATIONS:
				return basicSetEventDeclarations(null, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case LtsPackage.LTS_NETWORK__LTSS:
				return getLtss();
			case LtsPackage.LTS_NETWORK__EVENT_DECLARATIONS:
				return getEventDeclarations();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case LtsPackage.LTS_NETWORK__LTSS:
				getLtss().clear();
				getLtss().addAll((Collection<? extends LTS>)newValue);
				return;
			case LtsPackage.LTS_NETWORK__EVENT_DECLARATIONS:
				setEventDeclarations((EventDeclarations)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case LtsPackage.LTS_NETWORK__LTSS:
				getLtss().clear();
				return;
			case LtsPackage.LTS_NETWORK__EVENT_DECLARATIONS:
				setEventDeclarations((EventDeclarations)null);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case LtsPackage.LTS_NETWORK__LTSS:
				return ltss != null && !ltss.isEmpty();
			case LtsPackage.LTS_NETWORK__EVENT_DECLARATIONS:
				return eventDeclarations != null;
		}
		return super.eIsSet(featureID);
	}

} //LTSNetworkImpl
