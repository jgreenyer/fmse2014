/**
 */
package de.luh.se.fmse.lts;

import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.common.util.EMap;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>State</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link de.luh.se.fmse.lts.State#getOutgoingTransitions <em>Outgoing Transitions</em>}</li>
 *   <li>{@link de.luh.se.fmse.lts.State#getIncomingTransitions <em>Incoming Transitions</em>}</li>
 *   <li>{@link de.luh.se.fmse.lts.State#getContainingLTS <em>Containing LTS</em>}</li>
 * </ul>
 * </p>
 *
 * @see de.luh.se.fmse.lts.LtsPackage#getState()
 * @model
 * @generated
 */
public interface State extends NamedElement {
	/**
	 * Returns the value of the '<em><b>Outgoing Transitions</b></em>' containment reference list.
	 * The list contents are of type {@link de.luh.se.fmse.lts.Transition}.
	 * It is bidirectional and its opposite is '{@link de.luh.se.fmse.lts.Transition#getSourceState <em>Source State</em>}'.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Outgoing Transitions</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Outgoing Transitions</em>' containment reference list.
	 * @see de.luh.se.fmse.lts.LtsPackage#getState_OutgoingTransitions()
	 * @see de.luh.se.fmse.lts.Transition#getSourceState
	 * @model opposite="sourceState" containment="true"
	 * @generated
	 */
	EList<Transition> getOutgoingTransitions();

	/**
	 * Returns the value of the '<em><b>Incoming Transitions</b></em>' reference list.
	 * The list contents are of type {@link de.luh.se.fmse.lts.Transition}.
	 * It is bidirectional and its opposite is '{@link de.luh.se.fmse.lts.Transition#getTargetState <em>Target State</em>}'.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Incoming Transitions</em>' reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Incoming Transitions</em>' reference list.
	 * @see de.luh.se.fmse.lts.LtsPackage#getState_IncomingTransitions()
	 * @see de.luh.se.fmse.lts.Transition#getTargetState
	 * @model opposite="targetState"
	 * @generated
	 */
	EList<Transition> getIncomingTransitions();

	/**
	 * Returns the value of the '<em><b>Containing LTS</b></em>' container reference.
	 * It is bidirectional and its opposite is '{@link de.luh.se.fmse.lts.LTS#getStates <em>States</em>}'.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Containing LTS</em>' container reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Containing LTS</em>' container reference.
	 * @see #setContainingLTS(LTS)
	 * @see de.luh.se.fmse.lts.LtsPackage#getState_ContainingLTS()
	 * @see de.luh.se.fmse.lts.LTS#getStates
	 * @model opposite="states" required="true" transient="false"
	 * @generated
	 */
	LTS getContainingLTS();

	/**
	 * Sets the value of the '{@link de.luh.se.fmse.lts.State#getContainingLTS <em>Containing LTS</em>}' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Containing LTS</em>' container reference.
	 * @see #getContainingLTS()
	 * @generated
	 */
	void setContainingLTS(LTS value);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model mapType="de.luh.se.fmse.lts.StateListToComposedStateMapEntry<de.luh.se.fmse.lts.State, de.luh.se.fmse.lts.State>" sharedEventsMany="true"
	 * @generated
	 */
	EMap<EList<State>, State> createOutgoingTransitions(EList<Event> sharedEvents, State constituentState1, State constituentState2);

} // State
