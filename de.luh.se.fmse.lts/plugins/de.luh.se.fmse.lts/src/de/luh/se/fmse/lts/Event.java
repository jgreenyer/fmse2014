/**
 */
package de.luh.se.fmse.lts;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Event</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see de.luh.se.fmse.lts.LtsPackage#getEvent()
 * @model
 * @generated
 */
public interface Event extends NamedElement {
} // Event
