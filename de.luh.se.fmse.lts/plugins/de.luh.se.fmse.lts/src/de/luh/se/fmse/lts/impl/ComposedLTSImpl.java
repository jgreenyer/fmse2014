/**
 */
package de.luh.se.fmse.lts.impl;

import java.lang.reflect.InvocationTargetException;
import java.util.Collection;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import java.util.Stack;
import org.eclipse.emf.common.notify.NotificationChain;
import org.eclipse.emf.common.util.BasicEList;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.common.util.EMap;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EStructuralFeature;
import org.eclipse.emf.ecore.InternalEObject;
import org.eclipse.emf.ecore.util.EObjectResolvingEList;
import org.eclipse.emf.ecore.util.EcoreEMap;
import org.eclipse.emf.ecore.util.InternalEList;
import de.luh.se.fmse.lts.Alphabet;
import de.luh.se.fmse.lts.ComposedLTS;
import de.luh.se.fmse.lts.Event;
import de.luh.se.fmse.lts.LTS;
import de.luh.se.fmse.lts.LtsFactory;
import de.luh.se.fmse.lts.LtsPackage;
import de.luh.se.fmse.lts.State;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Composed LTS</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * <ul>
 *   <li>{@link de.luh.se.fmse.lts.impl.ComposedLTSImpl#getConstituentLTSs <em>Constituent LT Ss</em>}</li>
 *   <li>{@link de.luh.se.fmse.lts.impl.ComposedLTSImpl#getStateListToComposedStateMap <em>State List To Composed State Map</em>}</li>
 * </ul>
 * </p>
 *
 * @generated
 */
public class ComposedLTSImpl extends LTSImpl implements ComposedLTS {
	/**
	 * The cached value of the '{@link #getConstituentLTSs() <em>Constituent LT Ss</em>}' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getConstituentLTSs()
	 * @generated
	 * @ordered
	 */
	protected EList<LTS> constituentLTSs;
	/**
	 * The cached value of the '{@link #getStateListToComposedStateMap() <em>State List To Composed State Map</em>}' map.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getStateListToComposedStateMap()
	 * @generated
	 * @ordered
	 */
	protected EMap<EList<State>, State> stateListToComposedStateMap;
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected ComposedLTSImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return LtsPackage.Literals.COMPOSED_LTS;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<LTS> getConstituentLTSs() {
		if (constituentLTSs == null) {
			constituentLTSs = new EObjectResolvingEList<LTS>(LTS.class, this, LtsPackage.COMPOSED_LTS__CONSTITUENT_LT_SS);
		}
		return constituentLTSs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EMap<EList<State>, State> getStateListToComposedStateMap() {
		if (stateListToComposedStateMap == null) {
			stateListToComposedStateMap = new EcoreEMap<EList<State>,State>(LtsPackage.Literals.STATE_LIST_TO_COMPOSED_STATE_MAP_ENTRY, StateListToComposedStateMapEntryImpl.class, this, LtsPackage.COMPOSED_LTS__STATE_LIST_TO_COMPOSED_STATE_MAP);
		}
		return stateListToComposedStateMap;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public void compute() {

		compute(true);
		
	}
	
	
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public void compute(boolean computeUnreachableStates) {

		
		// start from scratch: clear states, which also removes transitions attached to the states.
		getStates().clear();
		
		
		// Used for a fast lookup of already existing states. See register and lookup functions.
		getStateListToComposedStateMap().clear();
		

		///////////////////////
		// Create Alphabet and compute shared events
		///////////////////////
		
		// get the two constituent LTSs
		LTS lts1 = getConstituentLTSs().get(0);
		LTS lts2 = getConstituentLTSs().get(1);
		
		// if they are also composed LTSs, compute them first.
		if (lts1 instanceof ComposedLTS 
				&& !lts1.equals(this)) // avoid infinite recursion
			((ComposedLTS)lts1).compute(computeUnreachableStates);
		if (lts2 instanceof ComposedLTS 
				&& !lts2.equals(this))
			((ComposedLTS)lts2).compute(computeUnreachableStates);
		
		// alphabet of composed LTS is union of alphabets of constituent LTSs
		Alphabet unionAlphabet = LtsPackage.eINSTANCE.getLtsFactory().createAlphabet();
		setAlphabet(unionAlphabet);
		// the set of shared events is required later on to determine which transitions must be synchronized
		EList<Event> sharedEvents = null;
		for (LTS lts : getConstituentLTSs()) {
			unionAlphabet.getEvents().addAll(lts.getAlphabet().getEvents());
			if (sharedEvents == null){
				sharedEvents = new BasicEList<Event>();
				sharedEvents.addAll(lts.getAlphabet().getEvents());
			}else{
				sharedEvents.retainAll(lts.getAlphabet().getEvents());
			}
		}
		
		
		///////////////////////
		// Compute composition: Here are two different versions
		///////////////////////
		// 1: Creates all composed states, then iterate over all of them an create
		//    outgoing transition
		// 2: First, only create initial composed state, then explore other reachable
		//    composed states from there
		///////////////////////		

		if(computeUnreachableStates){
		
			///////////////////////
			// Create composed states
			///////////////////////
	
			for (State state1 : lts1.getStates()) {
				for (State state2 : lts2.getStates()) {
					State composedState = LtsPackage.eINSTANCE.getLtsFactory().createState();
					// put them in a hash map for faster lookup later.
					registerComposedState(composedState, state1, state2);
					getStates().add(composedState);
					setDefaultCompositeNameForState(composedState, state1, state2);
					if(state1.getContainingLTS().getStartState().equals(state1)
							&& state2.getContainingLTS().getStartState().equals(state2))
						setStartState(composedState);
				}
			}


			///////////////////////
			// Create transitions
			///////////////////////
			for (Map.Entry<EList<State>, State> stateListToComposedStateMapEntry : getStateListToComposedStateMap().entrySet()) {
				State constituentState1 = stateListToComposedStateMapEntry.getKey().get(0);
				State constituentState2 = stateListToComposedStateMapEntry.getKey().get(1);
				stateListToComposedStateMapEntry.getValue().
					createOutgoingTransitions(sharedEvents, constituentState1, constituentState2);
			}
			
			
			
		}else{   

			///////////////////////
			// do not compute unreachable states
			///////////////////////

			// Stack for depth-first exploration of states
			// Instead of the composed states themselves, we store the list of states they consist of.
			// This is more convenient, because we need the constituent states for computing the outgoing transitions. 
			Stack<EList<State>> constituentStateListOfComposedStatesWaitingToBeExplored = new Stack<EList<State>>();
			
			// Set for marking already explored states
			Set<State> exploredStates = new HashSet<State>();
			
			///////////////////////
			// Create initial composed state
			///////////////////////
			
			State composedLTSStartState = LtsPackage.eINSTANCE.getLtsFactory().createState();
			getStates().add(composedLTSStartState);
			setDefaultCompositeNameForState(composedLTSStartState, lts1.getStartState(), lts2.getStartState());
			setStartState(composedLTSStartState);
			Map.Entry<EList<State>, State> composedStateMapEntry = registerComposedState(composedLTSStartState, lts1.getStartState(), lts2.getStartState());
			constituentStateListOfComposedStatesWaitingToBeExplored.push(composedStateMapEntry.getKey());
			
			///////////////////////
			// Explore other reachable composed states (depth-first, using a stack)
			///////////////////////
			
			while(!constituentStateListOfComposedStatesWaitingToBeExplored.isEmpty()){
				// pop state from stack
				EList<State> composedStateKeyList = constituentStateListOfComposedStatesWaitingToBeExplored.pop();
				State composedStateToExplore = getStateListToComposedStateMap().get(composedStateKeyList);
				
				if(!exploredStates.contains(composedStateToExplore)){ // do not explore an already explored state

					// create all outgoing transitions
					// (here it's important to know the constituent states, hence the stack is of type Stack<EList<State>>)
					EList<Map.Entry<EList<State>, State>> targetStatesMapEntries = composedStateToExplore.createOutgoingTransitions(sharedEvents,
							composedStateKeyList.get(0),
							composedStateKeyList.get(1));
					
					// mark composite state as explored
					exploredStates.add(composedStateToExplore);

					// add newly explored states on the stack if they were not already explored.
					for (Map.Entry<EList<State>, State> targetStatesMapEntry : targetStatesMapEntries) {
						if (!exploredStates.contains(targetStatesMapEntry.getValue())){
							constituentStateListOfComposedStatesWaitingToBeExplored.push(targetStatesMapEntry.getKey());
						}
					}
					
				}

			}
			
		}
			
			
	}

	
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public Map.Entry<EList<State>, State> registerComposedState(State composedState, State constituentState1, State constituentState2) {
		EList<State> keyList = new BasicEList<State>();
		keyList.add(constituentState1);
		keyList.add(constituentState2);
		getStateListToComposedStateMap().put(keyList, composedState);
		StateListToComposedStateMapEntryImpl composedStateMapEntry = new StateListToComposedStateMapEntryImpl();
		composedStateMapEntry.setKey(keyList);
		composedStateMapEntry.setValue(composedState);
		return composedStateMapEntry;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public Map.Entry<EList<State>, State> lookupOrCreateComposedState(State constituentState1, State constituentState2) {
		EList<State> constituentStatesList = new BasicEList<State>();
		constituentStatesList.add(constituentState1);
		constituentStatesList.add(constituentState2);		
		//System.out.println(constituentStatesList);
		Map.Entry<EList<State>, State> composedStateMapEntry;
		State lookedUpComposedState = getStateListToComposedStateMap().get(constituentStatesList);
		if (lookedUpComposedState != null){
			composedStateMapEntry = new StateListToComposedStateMapEntryImpl();
			((StateListToComposedStateMapEntryImpl)composedStateMapEntry).setKey(constituentStatesList);
			((StateListToComposedStateMapEntryImpl)composedStateMapEntry).setValue(lookedUpComposedState);
		}else{
			lookedUpComposedState = LtsFactory.eINSTANCE.createState();
			getStates().add(lookedUpComposedState);
			setDefaultCompositeNameForState(lookedUpComposedState, constituentState1, constituentState2);
			composedStateMapEntry = registerComposedState(lookedUpComposedState, constituentState1, constituentState2);
		}
		return composedStateMapEntry;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case LtsPackage.COMPOSED_LTS__STATE_LIST_TO_COMPOSED_STATE_MAP:
				return ((InternalEList<?>)getStateListToComposedStateMap()).basicRemove(otherEnd, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}
	
	
	private void setDefaultCompositeNameForState(State composedState, State constituentState1, State constituentState2){
		composedState.setName(constituentState1.getName() + ", " + constituentState2.getName());
	}
	
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case LtsPackage.COMPOSED_LTS__CONSTITUENT_LT_SS:
				return getConstituentLTSs();
			case LtsPackage.COMPOSED_LTS__STATE_LIST_TO_COMPOSED_STATE_MAP:
				if (coreType) return getStateListToComposedStateMap();
				else return getStateListToComposedStateMap().map();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case LtsPackage.COMPOSED_LTS__CONSTITUENT_LT_SS:
				getConstituentLTSs().clear();
				getConstituentLTSs().addAll((Collection<? extends LTS>)newValue);
				return;
			case LtsPackage.COMPOSED_LTS__STATE_LIST_TO_COMPOSED_STATE_MAP:
				((EStructuralFeature.Setting)getStateListToComposedStateMap()).set(newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case LtsPackage.COMPOSED_LTS__CONSTITUENT_LT_SS:
				getConstituentLTSs().clear();
				return;
			case LtsPackage.COMPOSED_LTS__STATE_LIST_TO_COMPOSED_STATE_MAP:
				getStateListToComposedStateMap().clear();
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case LtsPackage.COMPOSED_LTS__CONSTITUENT_LT_SS:
				return constituentLTSs != null && !constituentLTSs.isEmpty();
			case LtsPackage.COMPOSED_LTS__STATE_LIST_TO_COMPOSED_STATE_MAP:
				return stateListToComposedStateMap != null && !stateListToComposedStateMap.isEmpty();
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eInvoke(int operationID, EList<?> arguments) throws InvocationTargetException {
		switch (operationID) {
			case LtsPackage.COMPOSED_LTS___COMPUTE:
				compute();
				return null;
			case LtsPackage.COMPOSED_LTS___COMPUTE__BOOLEAN:
				compute((Boolean)arguments.get(0));
				return null;
			case LtsPackage.COMPOSED_LTS___REGISTER_COMPOSED_STATE__STATE_STATE_STATE:
				return registerComposedState((State)arguments.get(0), (State)arguments.get(1), (State)arguments.get(2));
			case LtsPackage.COMPOSED_LTS___LOOKUP_OR_CREATE_COMPOSED_STATE__STATE_STATE:
				return lookupOrCreateComposedState((State)arguments.get(0), (State)arguments.get(1));
		}
		return super.eInvoke(operationID, arguments);
	}

} //ComposedLTSImpl
