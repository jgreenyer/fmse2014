/**
 */
package de.luh.se.fmse.lts;

import org.eclipse.emf.common.util.EList;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>LTS Network</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link de.luh.se.fmse.lts.LTSNetwork#getLtss <em>Ltss</em>}</li>
 *   <li>{@link de.luh.se.fmse.lts.LTSNetwork#getEventDeclarations <em>Event Declarations</em>}</li>
 * </ul>
 * </p>
 *
 * @see de.luh.se.fmse.lts.LtsPackage#getLTSNetwork()
 * @model
 * @generated
 */
public interface LTSNetwork extends NamedElement {
	/**
	 * Returns the value of the '<em><b>Ltss</b></em>' containment reference list.
	 * The list contents are of type {@link de.luh.se.fmse.lts.LTS}.
	 * It is bidirectional and its opposite is '{@link de.luh.se.fmse.lts.LTS#getLtsnetwork <em>Ltsnetwork</em>}'.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Ltss</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Ltss</em>' containment reference list.
	 * @see de.luh.se.fmse.lts.LtsPackage#getLTSNetwork_Ltss()
	 * @see de.luh.se.fmse.lts.LTS#getLtsnetwork
	 * @model opposite="ltsnetwork" containment="true"
	 * @generated
	 */
	EList<LTS> getLtss();

	/**
	 * Returns the value of the '<em><b>Event Declarations</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Event Declarations</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Event Declarations</em>' containment reference.
	 * @see #setEventDeclarations(EventDeclarations)
	 * @see de.luh.se.fmse.lts.LtsPackage#getLTSNetwork_EventDeclarations()
	 * @model containment="true" required="true"
	 * @generated
	 */
	EventDeclarations getEventDeclarations();

	/**
	 * Sets the value of the '{@link de.luh.se.fmse.lts.LTSNetwork#getEventDeclarations <em>Event Declarations</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Event Declarations</em>' containment reference.
	 * @see #getEventDeclarations()
	 * @generated
	 */
	void setEventDeclarations(EventDeclarations value);

} // LTSNetwork
