/**
 */
package de.luh.se.fmse.lts;

import java.util.Map;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.common.util.EMap;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Composed LTS</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link de.luh.se.fmse.lts.ComposedLTS#getConstituentLTSs <em>Constituent LT Ss</em>}</li>
 *   <li>{@link de.luh.se.fmse.lts.ComposedLTS#getStateListToComposedStateMap <em>State List To Composed State Map</em>}</li>
 * </ul>
 * </p>
 *
 * @see de.luh.se.fmse.lts.LtsPackage#getComposedLTS()
 * @model
 * @generated
 */
public interface ComposedLTS extends LTS {
	/**
	 * Returns the value of the '<em><b>Constituent LT Ss</b></em>' reference list.
	 * The list contents are of type {@link de.luh.se.fmse.lts.LTS}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Constituent LT Ss</em>' reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Constituent LT Ss</em>' reference list.
	 * @see de.luh.se.fmse.lts.LtsPackage#getComposedLTS_ConstituentLTSs()
	 * @model
	 * @generated
	 */
	EList<LTS> getConstituentLTSs();

	/**
	 * Returns the value of the '<em><b>State List To Composed State Map</b></em>' map.
	 * The key is of type list of {@link de.luh.se.fmse.lts.State},
	 * and the value is of type {@link de.luh.se.fmse.lts.State},
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>State List To Composed State Map</em>' reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>State List To Composed State Map</em>' map.
	 * @see de.luh.se.fmse.lts.LtsPackage#getComposedLTS_StateListToComposedStateMap()
	 * @model mapType="de.luh.se.fmse.lts.StateListToComposedStateMapEntry<de.luh.se.fmse.lts.State, de.luh.se.fmse.lts.State>"
	 * @generated
	 */
	EMap<EList<State>, State> getStateListToComposedStateMap();

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model
	 * @generated
	 */
	void compute();

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model
	 * @generated
	 */
	void compute(boolean computeUnreachableStates);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model mapType="de.luh.se.fmse.lts.StateListToComposedStateMapEntry<de.luh.se.fmse.lts.State, de.luh.se.fmse.lts.State>"
	 * @generated
	 */
	Map.Entry<EList<State>, State> registerComposedState(State composedState, State constituentState1, State constituentState2);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model mapType="de.luh.se.fmse.lts.StateListToComposedStateMapEntry<de.luh.se.fmse.lts.State, de.luh.se.fmse.lts.State>"
	 * @generated
	 */
	Map.Entry<EList<State>, State> lookupOrCreateComposedState(State constituentState1, State constituentState2);

} // ComposedLTS
