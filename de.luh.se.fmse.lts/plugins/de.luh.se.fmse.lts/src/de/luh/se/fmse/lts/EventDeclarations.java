/**
 */
package de.luh.se.fmse.lts;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Event Declarations</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link de.luh.se.fmse.lts.EventDeclarations#getDeclaredEvents <em>Declared Events</em>}</li>
 * </ul>
 * </p>
 *
 * @see de.luh.se.fmse.lts.LtsPackage#getEventDeclarations()
 * @model
 * @generated
 */
public interface EventDeclarations extends EObject {
	/**
	 * Returns the value of the '<em><b>Declared Events</b></em>' containment reference list.
	 * The list contents are of type {@link de.luh.se.fmse.lts.Event}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Declared Events</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Declared Events</em>' containment reference list.
	 * @see de.luh.se.fmse.lts.LtsPackage#getEventDeclarations_DeclaredEvents()
	 * @model containment="true"
	 * @generated
	 */
	EList<Event> getDeclaredEvents();

} // EventDeclarations
