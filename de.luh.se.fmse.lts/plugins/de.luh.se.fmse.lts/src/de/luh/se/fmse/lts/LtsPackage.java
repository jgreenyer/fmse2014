/**
 */
package de.luh.se.fmse.lts;

import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EOperation;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.EReference;

/**
 * <!-- begin-user-doc -->
 * The <b>Package</b> for the model.
 * It contains accessors for the meta objects to represent
 * <ul>
 *   <li>each class,</li>
 *   <li>each feature of each class,</li>
 *   <li>each operation of each class,</li>
 *   <li>each enum,</li>
 *   <li>and each data type</li>
 * </ul>
 * <!-- end-user-doc -->
 * @see de.luh.se.fmse.lts.LtsFactory
 * @model kind="package"
 * @generated
 */
public interface LtsPackage extends EPackage {
	/**
	 * The package name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNAME = "lts";

	/**
	 * The package namespace URI.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNS_URI = "http://de.luh.se.fmse/lts";

	/**
	 * The package namespace name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNS_PREFIX = "de.luh.se.fmse";

	/**
	 * The singleton instance of the package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	LtsPackage eINSTANCE = de.luh.se.fmse.lts.impl.LtsPackageImpl.init();

	/**
	 * The meta object id for the '{@link de.luh.se.fmse.lts.impl.NamedElementImpl <em>Named Element</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see de.luh.se.fmse.lts.impl.NamedElementImpl
	 * @see de.luh.se.fmse.lts.impl.LtsPackageImpl#getNamedElement()
	 * @generated
	 */
	int NAMED_ELEMENT = 5;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int NAMED_ELEMENT__NAME = 0;

	/**
	 * The number of structural features of the '<em>Named Element</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int NAMED_ELEMENT_FEATURE_COUNT = 1;

	/**
	 * The number of operations of the '<em>Named Element</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int NAMED_ELEMENT_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link de.luh.se.fmse.lts.impl.LTSImpl <em>LTS</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see de.luh.se.fmse.lts.impl.LTSImpl
	 * @see de.luh.se.fmse.lts.impl.LtsPackageImpl#getLTS()
	 * @generated
	 */
	int LTS = 0;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LTS__NAME = NAMED_ELEMENT__NAME;

	/**
	 * The feature id for the '<em><b>Ltsnetwork</b></em>' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LTS__LTSNETWORK = NAMED_ELEMENT_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>States</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LTS__STATES = NAMED_ELEMENT_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>Alphabet</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LTS__ALPHABET = NAMED_ELEMENT_FEATURE_COUNT + 2;

	/**
	 * The feature id for the '<em><b>Start State</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LTS__START_STATE = NAMED_ELEMENT_FEATURE_COUNT + 3;

	/**
	 * The number of structural features of the '<em>LTS</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LTS_FEATURE_COUNT = NAMED_ELEMENT_FEATURE_COUNT + 4;

	/**
	 * The number of operations of the '<em>LTS</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LTS_OPERATION_COUNT = NAMED_ELEMENT_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link de.luh.se.fmse.lts.impl.ComposedLTSImpl <em>Composed LTS</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see de.luh.se.fmse.lts.impl.ComposedLTSImpl
	 * @see de.luh.se.fmse.lts.impl.LtsPackageImpl#getComposedLTS()
	 * @generated
	 */
	int COMPOSED_LTS = 1;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COMPOSED_LTS__NAME = LTS__NAME;

	/**
	 * The feature id for the '<em><b>Ltsnetwork</b></em>' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COMPOSED_LTS__LTSNETWORK = LTS__LTSNETWORK;

	/**
	 * The feature id for the '<em><b>States</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COMPOSED_LTS__STATES = LTS__STATES;

	/**
	 * The feature id for the '<em><b>Alphabet</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COMPOSED_LTS__ALPHABET = LTS__ALPHABET;

	/**
	 * The feature id for the '<em><b>Start State</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COMPOSED_LTS__START_STATE = LTS__START_STATE;

	/**
	 * The feature id for the '<em><b>Constituent LT Ss</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COMPOSED_LTS__CONSTITUENT_LT_SS = LTS_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>State List To Composed State Map</b></em>' map.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COMPOSED_LTS__STATE_LIST_TO_COMPOSED_STATE_MAP = LTS_FEATURE_COUNT + 1;

	/**
	 * The number of structural features of the '<em>Composed LTS</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COMPOSED_LTS_FEATURE_COUNT = LTS_FEATURE_COUNT + 2;

	/**
	 * The operation id for the '<em>Compute</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COMPOSED_LTS___COMPUTE = LTS_OPERATION_COUNT + 0;

	/**
	 * The operation id for the '<em>Compute</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COMPOSED_LTS___COMPUTE__BOOLEAN = LTS_OPERATION_COUNT + 1;

	/**
	 * The operation id for the '<em>Register Composed State</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COMPOSED_LTS___REGISTER_COMPOSED_STATE__STATE_STATE_STATE = LTS_OPERATION_COUNT + 2;

	/**
	 * The operation id for the '<em>Lookup Or Create Composed State</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COMPOSED_LTS___LOOKUP_OR_CREATE_COMPOSED_STATE__STATE_STATE = LTS_OPERATION_COUNT + 3;

	/**
	 * The number of operations of the '<em>Composed LTS</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COMPOSED_LTS_OPERATION_COUNT = LTS_OPERATION_COUNT + 4;

	/**
	 * The meta object id for the '{@link de.luh.se.fmse.lts.impl.LTSNetworkImpl <em>LTS Network</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see de.luh.se.fmse.lts.impl.LTSNetworkImpl
	 * @see de.luh.se.fmse.lts.impl.LtsPackageImpl#getLTSNetwork()
	 * @generated
	 */
	int LTS_NETWORK = 2;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LTS_NETWORK__NAME = NAMED_ELEMENT__NAME;

	/**
	 * The feature id for the '<em><b>Ltss</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LTS_NETWORK__LTSS = NAMED_ELEMENT_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Event Declarations</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LTS_NETWORK__EVENT_DECLARATIONS = NAMED_ELEMENT_FEATURE_COUNT + 1;

	/**
	 * The number of structural features of the '<em>LTS Network</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LTS_NETWORK_FEATURE_COUNT = NAMED_ELEMENT_FEATURE_COUNT + 2;

	/**
	 * The number of operations of the '<em>LTS Network</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LTS_NETWORK_OPERATION_COUNT = NAMED_ELEMENT_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link de.luh.se.fmse.lts.impl.StateImpl <em>State</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see de.luh.se.fmse.lts.impl.StateImpl
	 * @see de.luh.se.fmse.lts.impl.LtsPackageImpl#getState()
	 * @generated
	 */
	int STATE = 3;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int STATE__NAME = NAMED_ELEMENT__NAME;

	/**
	 * The feature id for the '<em><b>Outgoing Transitions</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int STATE__OUTGOING_TRANSITIONS = NAMED_ELEMENT_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Incoming Transitions</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int STATE__INCOMING_TRANSITIONS = NAMED_ELEMENT_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>Containing LTS</b></em>' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int STATE__CONTAINING_LTS = NAMED_ELEMENT_FEATURE_COUNT + 2;

	/**
	 * The number of structural features of the '<em>State</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int STATE_FEATURE_COUNT = NAMED_ELEMENT_FEATURE_COUNT + 3;

	/**
	 * The operation id for the '<em>Create Outgoing Transitions</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int STATE___CREATE_OUTGOING_TRANSITIONS__ELIST_STATE_STATE = NAMED_ELEMENT_OPERATION_COUNT + 0;

	/**
	 * The number of operations of the '<em>State</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int STATE_OPERATION_COUNT = NAMED_ELEMENT_OPERATION_COUNT + 1;

	/**
	 * The meta object id for the '{@link de.luh.se.fmse.lts.impl.TransitionImpl <em>Transition</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see de.luh.se.fmse.lts.impl.TransitionImpl
	 * @see de.luh.se.fmse.lts.impl.LtsPackageImpl#getTransition()
	 * @generated
	 */
	int TRANSITION = 4;

	/**
	 * The feature id for the '<em><b>Source State</b></em>' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TRANSITION__SOURCE_STATE = 0;

	/**
	 * The feature id for the '<em><b>Target State</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TRANSITION__TARGET_STATE = 1;

	/**
	 * The feature id for the '<em><b>Label</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TRANSITION__LABEL = 2;

	/**
	 * The number of structural features of the '<em>Transition</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TRANSITION_FEATURE_COUNT = 3;

	/**
	 * The number of operations of the '<em>Transition</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TRANSITION_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link de.luh.se.fmse.lts.impl.AlphabetImpl <em>Alphabet</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see de.luh.se.fmse.lts.impl.AlphabetImpl
	 * @see de.luh.se.fmse.lts.impl.LtsPackageImpl#getAlphabet()
	 * @generated
	 */
	int ALPHABET = 6;

	/**
	 * The feature id for the '<em><b>Events</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ALPHABET__EVENTS = 0;

	/**
	 * The number of structural features of the '<em>Alphabet</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ALPHABET_FEATURE_COUNT = 1;

	/**
	 * The number of operations of the '<em>Alphabet</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ALPHABET_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link de.luh.se.fmse.lts.impl.EventImpl <em>Event</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see de.luh.se.fmse.lts.impl.EventImpl
	 * @see de.luh.se.fmse.lts.impl.LtsPackageImpl#getEvent()
	 * @generated
	 */
	int EVENT = 7;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EVENT__NAME = NAMED_ELEMENT__NAME;

	/**
	 * The number of structural features of the '<em>Event</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EVENT_FEATURE_COUNT = NAMED_ELEMENT_FEATURE_COUNT + 0;

	/**
	 * The number of operations of the '<em>Event</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EVENT_OPERATION_COUNT = NAMED_ELEMENT_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link de.luh.se.fmse.lts.impl.EventDeclarationsImpl <em>Event Declarations</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see de.luh.se.fmse.lts.impl.EventDeclarationsImpl
	 * @see de.luh.se.fmse.lts.impl.LtsPackageImpl#getEventDeclarations()
	 * @generated
	 */
	int EVENT_DECLARATIONS = 8;

	/**
	 * The feature id for the '<em><b>Declared Events</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EVENT_DECLARATIONS__DECLARED_EVENTS = 0;

	/**
	 * The number of structural features of the '<em>Event Declarations</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EVENT_DECLARATIONS_FEATURE_COUNT = 1;

	/**
	 * The number of operations of the '<em>Event Declarations</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EVENT_DECLARATIONS_OPERATION_COUNT = 0;


	/**
	 * The meta object id for the '{@link de.luh.se.fmse.lts.impl.StateListToComposedStateMapEntryImpl <em>State List To Composed State Map Entry</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see de.luh.se.fmse.lts.impl.StateListToComposedStateMapEntryImpl
	 * @see de.luh.se.fmse.lts.impl.LtsPackageImpl#getStateListToComposedStateMapEntry()
	 * @generated
	 */
	int STATE_LIST_TO_COMPOSED_STATE_MAP_ENTRY = 9;

	/**
	 * The feature id for the '<em><b>Key</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int STATE_LIST_TO_COMPOSED_STATE_MAP_ENTRY__KEY = 0;

	/**
	 * The feature id for the '<em><b>Value</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int STATE_LIST_TO_COMPOSED_STATE_MAP_ENTRY__VALUE = 1;

	/**
	 * The number of structural features of the '<em>State List To Composed State Map Entry</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int STATE_LIST_TO_COMPOSED_STATE_MAP_ENTRY_FEATURE_COUNT = 2;

	/**
	 * The number of operations of the '<em>State List To Composed State Map Entry</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int STATE_LIST_TO_COMPOSED_STATE_MAP_ENTRY_OPERATION_COUNT = 0;


	/**
	 * Returns the meta object for class '{@link de.luh.se.fmse.lts.LTS <em>LTS</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>LTS</em>'.
	 * @see de.luh.se.fmse.lts.LTS
	 * @generated
	 */
	EClass getLTS();

	/**
	 * Returns the meta object for the container reference '{@link de.luh.se.fmse.lts.LTS#getLtsnetwork <em>Ltsnetwork</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the container reference '<em>Ltsnetwork</em>'.
	 * @see de.luh.se.fmse.lts.LTS#getLtsnetwork()
	 * @see #getLTS()
	 * @generated
	 */
	EReference getLTS_Ltsnetwork();

	/**
	 * Returns the meta object for the containment reference list '{@link de.luh.se.fmse.lts.LTS#getStates <em>States</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>States</em>'.
	 * @see de.luh.se.fmse.lts.LTS#getStates()
	 * @see #getLTS()
	 * @generated
	 */
	EReference getLTS_States();

	/**
	 * Returns the meta object for the containment reference '{@link de.luh.se.fmse.lts.LTS#getAlphabet <em>Alphabet</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Alphabet</em>'.
	 * @see de.luh.se.fmse.lts.LTS#getAlphabet()
	 * @see #getLTS()
	 * @generated
	 */
	EReference getLTS_Alphabet();

	/**
	 * Returns the meta object for the reference '{@link de.luh.se.fmse.lts.LTS#getStartState <em>Start State</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Start State</em>'.
	 * @see de.luh.se.fmse.lts.LTS#getStartState()
	 * @see #getLTS()
	 * @generated
	 */
	EReference getLTS_StartState();

	/**
	 * Returns the meta object for class '{@link de.luh.se.fmse.lts.ComposedLTS <em>Composed LTS</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Composed LTS</em>'.
	 * @see de.luh.se.fmse.lts.ComposedLTS
	 * @generated
	 */
	EClass getComposedLTS();

	/**
	 * Returns the meta object for the reference list '{@link de.luh.se.fmse.lts.ComposedLTS#getConstituentLTSs <em>Constituent LT Ss</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference list '<em>Constituent LT Ss</em>'.
	 * @see de.luh.se.fmse.lts.ComposedLTS#getConstituentLTSs()
	 * @see #getComposedLTS()
	 * @generated
	 */
	EReference getComposedLTS_ConstituentLTSs();

	/**
	 * Returns the meta object for the map '{@link de.luh.se.fmse.lts.ComposedLTS#getStateListToComposedStateMap <em>State List To Composed State Map</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the map '<em>State List To Composed State Map</em>'.
	 * @see de.luh.se.fmse.lts.ComposedLTS#getStateListToComposedStateMap()
	 * @see #getComposedLTS()
	 * @generated
	 */
	EReference getComposedLTS_StateListToComposedStateMap();

	/**
	 * Returns the meta object for the '{@link de.luh.se.fmse.lts.ComposedLTS#compute() <em>Compute</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Compute</em>' operation.
	 * @see de.luh.se.fmse.lts.ComposedLTS#compute()
	 * @generated
	 */
	EOperation getComposedLTS__Compute();

	/**
	 * Returns the meta object for the '{@link de.luh.se.fmse.lts.ComposedLTS#compute(boolean) <em>Compute</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Compute</em>' operation.
	 * @see de.luh.se.fmse.lts.ComposedLTS#compute(boolean)
	 * @generated
	 */
	EOperation getComposedLTS__Compute__boolean();

	/**
	 * Returns the meta object for the '{@link de.luh.se.fmse.lts.ComposedLTS#registerComposedState(de.luh.se.fmse.lts.State, de.luh.se.fmse.lts.State, de.luh.se.fmse.lts.State) <em>Register Composed State</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Register Composed State</em>' operation.
	 * @see de.luh.se.fmse.lts.ComposedLTS#registerComposedState(de.luh.se.fmse.lts.State, de.luh.se.fmse.lts.State, de.luh.se.fmse.lts.State)
	 * @generated
	 */
	EOperation getComposedLTS__RegisterComposedState__State_State_State();

	/**
	 * Returns the meta object for the '{@link de.luh.se.fmse.lts.ComposedLTS#lookupOrCreateComposedState(de.luh.se.fmse.lts.State, de.luh.se.fmse.lts.State) <em>Lookup Or Create Composed State</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Lookup Or Create Composed State</em>' operation.
	 * @see de.luh.se.fmse.lts.ComposedLTS#lookupOrCreateComposedState(de.luh.se.fmse.lts.State, de.luh.se.fmse.lts.State)
	 * @generated
	 */
	EOperation getComposedLTS__LookupOrCreateComposedState__State_State();

	/**
	 * Returns the meta object for class '{@link de.luh.se.fmse.lts.LTSNetwork <em>LTS Network</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>LTS Network</em>'.
	 * @see de.luh.se.fmse.lts.LTSNetwork
	 * @generated
	 */
	EClass getLTSNetwork();

	/**
	 * Returns the meta object for the containment reference list '{@link de.luh.se.fmse.lts.LTSNetwork#getLtss <em>Ltss</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Ltss</em>'.
	 * @see de.luh.se.fmse.lts.LTSNetwork#getLtss()
	 * @see #getLTSNetwork()
	 * @generated
	 */
	EReference getLTSNetwork_Ltss();

	/**
	 * Returns the meta object for the containment reference '{@link de.luh.se.fmse.lts.LTSNetwork#getEventDeclarations <em>Event Declarations</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Event Declarations</em>'.
	 * @see de.luh.se.fmse.lts.LTSNetwork#getEventDeclarations()
	 * @see #getLTSNetwork()
	 * @generated
	 */
	EReference getLTSNetwork_EventDeclarations();

	/**
	 * Returns the meta object for class '{@link de.luh.se.fmse.lts.State <em>State</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>State</em>'.
	 * @see de.luh.se.fmse.lts.State
	 * @generated
	 */
	EClass getState();

	/**
	 * Returns the meta object for the containment reference list '{@link de.luh.se.fmse.lts.State#getOutgoingTransitions <em>Outgoing Transitions</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Outgoing Transitions</em>'.
	 * @see de.luh.se.fmse.lts.State#getOutgoingTransitions()
	 * @see #getState()
	 * @generated
	 */
	EReference getState_OutgoingTransitions();

	/**
	 * Returns the meta object for the reference list '{@link de.luh.se.fmse.lts.State#getIncomingTransitions <em>Incoming Transitions</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference list '<em>Incoming Transitions</em>'.
	 * @see de.luh.se.fmse.lts.State#getIncomingTransitions()
	 * @see #getState()
	 * @generated
	 */
	EReference getState_IncomingTransitions();

	/**
	 * Returns the meta object for the container reference '{@link de.luh.se.fmse.lts.State#getContainingLTS <em>Containing LTS</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the container reference '<em>Containing LTS</em>'.
	 * @see de.luh.se.fmse.lts.State#getContainingLTS()
	 * @see #getState()
	 * @generated
	 */
	EReference getState_ContainingLTS();

	/**
	 * Returns the meta object for the '{@link de.luh.se.fmse.lts.State#createOutgoingTransitions(org.eclipse.emf.common.util.EList, de.luh.se.fmse.lts.State, de.luh.se.fmse.lts.State) <em>Create Outgoing Transitions</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Create Outgoing Transitions</em>' operation.
	 * @see de.luh.se.fmse.lts.State#createOutgoingTransitions(org.eclipse.emf.common.util.EList, de.luh.se.fmse.lts.State, de.luh.se.fmse.lts.State)
	 * @generated
	 */
	EOperation getState__CreateOutgoingTransitions__EList_State_State();

	/**
	 * Returns the meta object for class '{@link de.luh.se.fmse.lts.Transition <em>Transition</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Transition</em>'.
	 * @see de.luh.se.fmse.lts.Transition
	 * @generated
	 */
	EClass getTransition();

	/**
	 * Returns the meta object for the container reference '{@link de.luh.se.fmse.lts.Transition#getSourceState <em>Source State</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the container reference '<em>Source State</em>'.
	 * @see de.luh.se.fmse.lts.Transition#getSourceState()
	 * @see #getTransition()
	 * @generated
	 */
	EReference getTransition_SourceState();

	/**
	 * Returns the meta object for the reference '{@link de.luh.se.fmse.lts.Transition#getTargetState <em>Target State</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Target State</em>'.
	 * @see de.luh.se.fmse.lts.Transition#getTargetState()
	 * @see #getTransition()
	 * @generated
	 */
	EReference getTransition_TargetState();

	/**
	 * Returns the meta object for the reference '{@link de.luh.se.fmse.lts.Transition#getLabel <em>Label</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Label</em>'.
	 * @see de.luh.se.fmse.lts.Transition#getLabel()
	 * @see #getTransition()
	 * @generated
	 */
	EReference getTransition_Label();

	/**
	 * Returns the meta object for class '{@link de.luh.se.fmse.lts.NamedElement <em>Named Element</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Named Element</em>'.
	 * @see de.luh.se.fmse.lts.NamedElement
	 * @generated
	 */
	EClass getNamedElement();

	/**
	 * Returns the meta object for the attribute '{@link de.luh.se.fmse.lts.NamedElement#getName <em>Name</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Name</em>'.
	 * @see de.luh.se.fmse.lts.NamedElement#getName()
	 * @see #getNamedElement()
	 * @generated
	 */
	EAttribute getNamedElement_Name();

	/**
	 * Returns the meta object for class '{@link de.luh.se.fmse.lts.Alphabet <em>Alphabet</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Alphabet</em>'.
	 * @see de.luh.se.fmse.lts.Alphabet
	 * @generated
	 */
	EClass getAlphabet();

	/**
	 * Returns the meta object for the reference list '{@link de.luh.se.fmse.lts.Alphabet#getEvents <em>Events</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference list '<em>Events</em>'.
	 * @see de.luh.se.fmse.lts.Alphabet#getEvents()
	 * @see #getAlphabet()
	 * @generated
	 */
	EReference getAlphabet_Events();

	/**
	 * Returns the meta object for class '{@link de.luh.se.fmse.lts.Event <em>Event</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Event</em>'.
	 * @see de.luh.se.fmse.lts.Event
	 * @generated
	 */
	EClass getEvent();

	/**
	 * Returns the meta object for class '{@link de.luh.se.fmse.lts.EventDeclarations <em>Event Declarations</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Event Declarations</em>'.
	 * @see de.luh.se.fmse.lts.EventDeclarations
	 * @generated
	 */
	EClass getEventDeclarations();

	/**
	 * Returns the meta object for the containment reference list '{@link de.luh.se.fmse.lts.EventDeclarations#getDeclaredEvents <em>Declared Events</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Declared Events</em>'.
	 * @see de.luh.se.fmse.lts.EventDeclarations#getDeclaredEvents()
	 * @see #getEventDeclarations()
	 * @generated
	 */
	EReference getEventDeclarations_DeclaredEvents();

	/**
	 * Returns the meta object for class '{@link java.util.Map.Entry <em>State List To Composed State Map Entry</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>State List To Composed State Map Entry</em>'.
	 * @see java.util.Map.Entry
	 * @model keyType="de.luh.se.fmse.lts.State" keyMany="true"
	 *        valueType="de.luh.se.fmse.lts.State"
	 * @generated
	 */
	EClass getStateListToComposedStateMapEntry();

	/**
	 * Returns the meta object for the reference list '{@link java.util.Map.Entry <em>Key</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference list '<em>Key</em>'.
	 * @see java.util.Map.Entry
	 * @see #getStateListToComposedStateMapEntry()
	 * @generated
	 */
	EReference getStateListToComposedStateMapEntry_Key();

	/**
	 * Returns the meta object for the reference '{@link java.util.Map.Entry <em>Value</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Value</em>'.
	 * @see java.util.Map.Entry
	 * @see #getStateListToComposedStateMapEntry()
	 * @generated
	 */
	EReference getStateListToComposedStateMapEntry_Value();

	/**
	 * Returns the factory that creates the instances of the model.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the factory that creates the instances of the model.
	 * @generated
	 */
	LtsFactory getLtsFactory();

	/**
	 * <!-- begin-user-doc -->
	 * Defines literals for the meta objects that represent
	 * <ul>
	 *   <li>each class,</li>
	 *   <li>each feature of each class,</li>
	 *   <li>each operation of each class,</li>
	 *   <li>each enum,</li>
	 *   <li>and each data type</li>
	 * </ul>
	 * <!-- end-user-doc -->
	 * @generated
	 */
	interface Literals {
		/**
		 * The meta object literal for the '{@link de.luh.se.fmse.lts.impl.LTSImpl <em>LTS</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see de.luh.se.fmse.lts.impl.LTSImpl
		 * @see de.luh.se.fmse.lts.impl.LtsPackageImpl#getLTS()
		 * @generated
		 */
		EClass LTS = eINSTANCE.getLTS();

		/**
		 * The meta object literal for the '<em><b>Ltsnetwork</b></em>' container reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference LTS__LTSNETWORK = eINSTANCE.getLTS_Ltsnetwork();

		/**
		 * The meta object literal for the '<em><b>States</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference LTS__STATES = eINSTANCE.getLTS_States();

		/**
		 * The meta object literal for the '<em><b>Alphabet</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference LTS__ALPHABET = eINSTANCE.getLTS_Alphabet();

		/**
		 * The meta object literal for the '<em><b>Start State</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference LTS__START_STATE = eINSTANCE.getLTS_StartState();

		/**
		 * The meta object literal for the '{@link de.luh.se.fmse.lts.impl.ComposedLTSImpl <em>Composed LTS</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see de.luh.se.fmse.lts.impl.ComposedLTSImpl
		 * @see de.luh.se.fmse.lts.impl.LtsPackageImpl#getComposedLTS()
		 * @generated
		 */
		EClass COMPOSED_LTS = eINSTANCE.getComposedLTS();

		/**
		 * The meta object literal for the '<em><b>Constituent LT Ss</b></em>' reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference COMPOSED_LTS__CONSTITUENT_LT_SS = eINSTANCE.getComposedLTS_ConstituentLTSs();

		/**
		 * The meta object literal for the '<em><b>State List To Composed State Map</b></em>' map feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference COMPOSED_LTS__STATE_LIST_TO_COMPOSED_STATE_MAP = eINSTANCE.getComposedLTS_StateListToComposedStateMap();

		/**
		 * The meta object literal for the '<em><b>Compute</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation COMPOSED_LTS___COMPUTE = eINSTANCE.getComposedLTS__Compute();

		/**
		 * The meta object literal for the '<em><b>Compute</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation COMPOSED_LTS___COMPUTE__BOOLEAN = eINSTANCE.getComposedLTS__Compute__boolean();

		/**
		 * The meta object literal for the '<em><b>Register Composed State</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation COMPOSED_LTS___REGISTER_COMPOSED_STATE__STATE_STATE_STATE = eINSTANCE.getComposedLTS__RegisterComposedState__State_State_State();

		/**
		 * The meta object literal for the '<em><b>Lookup Or Create Composed State</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation COMPOSED_LTS___LOOKUP_OR_CREATE_COMPOSED_STATE__STATE_STATE = eINSTANCE.getComposedLTS__LookupOrCreateComposedState__State_State();

		/**
		 * The meta object literal for the '{@link de.luh.se.fmse.lts.impl.LTSNetworkImpl <em>LTS Network</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see de.luh.se.fmse.lts.impl.LTSNetworkImpl
		 * @see de.luh.se.fmse.lts.impl.LtsPackageImpl#getLTSNetwork()
		 * @generated
		 */
		EClass LTS_NETWORK = eINSTANCE.getLTSNetwork();

		/**
		 * The meta object literal for the '<em><b>Ltss</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference LTS_NETWORK__LTSS = eINSTANCE.getLTSNetwork_Ltss();

		/**
		 * The meta object literal for the '<em><b>Event Declarations</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference LTS_NETWORK__EVENT_DECLARATIONS = eINSTANCE.getLTSNetwork_EventDeclarations();

		/**
		 * The meta object literal for the '{@link de.luh.se.fmse.lts.impl.StateImpl <em>State</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see de.luh.se.fmse.lts.impl.StateImpl
		 * @see de.luh.se.fmse.lts.impl.LtsPackageImpl#getState()
		 * @generated
		 */
		EClass STATE = eINSTANCE.getState();

		/**
		 * The meta object literal for the '<em><b>Outgoing Transitions</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference STATE__OUTGOING_TRANSITIONS = eINSTANCE.getState_OutgoingTransitions();

		/**
		 * The meta object literal for the '<em><b>Incoming Transitions</b></em>' reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference STATE__INCOMING_TRANSITIONS = eINSTANCE.getState_IncomingTransitions();

		/**
		 * The meta object literal for the '<em><b>Containing LTS</b></em>' container reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference STATE__CONTAINING_LTS = eINSTANCE.getState_ContainingLTS();

		/**
		 * The meta object literal for the '<em><b>Create Outgoing Transitions</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation STATE___CREATE_OUTGOING_TRANSITIONS__ELIST_STATE_STATE = eINSTANCE.getState__CreateOutgoingTransitions__EList_State_State();

		/**
		 * The meta object literal for the '{@link de.luh.se.fmse.lts.impl.TransitionImpl <em>Transition</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see de.luh.se.fmse.lts.impl.TransitionImpl
		 * @see de.luh.se.fmse.lts.impl.LtsPackageImpl#getTransition()
		 * @generated
		 */
		EClass TRANSITION = eINSTANCE.getTransition();

		/**
		 * The meta object literal for the '<em><b>Source State</b></em>' container reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference TRANSITION__SOURCE_STATE = eINSTANCE.getTransition_SourceState();

		/**
		 * The meta object literal for the '<em><b>Target State</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference TRANSITION__TARGET_STATE = eINSTANCE.getTransition_TargetState();

		/**
		 * The meta object literal for the '<em><b>Label</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference TRANSITION__LABEL = eINSTANCE.getTransition_Label();

		/**
		 * The meta object literal for the '{@link de.luh.se.fmse.lts.impl.NamedElementImpl <em>Named Element</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see de.luh.se.fmse.lts.impl.NamedElementImpl
		 * @see de.luh.se.fmse.lts.impl.LtsPackageImpl#getNamedElement()
		 * @generated
		 */
		EClass NAMED_ELEMENT = eINSTANCE.getNamedElement();

		/**
		 * The meta object literal for the '<em><b>Name</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute NAMED_ELEMENT__NAME = eINSTANCE.getNamedElement_Name();

		/**
		 * The meta object literal for the '{@link de.luh.se.fmse.lts.impl.AlphabetImpl <em>Alphabet</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see de.luh.se.fmse.lts.impl.AlphabetImpl
		 * @see de.luh.se.fmse.lts.impl.LtsPackageImpl#getAlphabet()
		 * @generated
		 */
		EClass ALPHABET = eINSTANCE.getAlphabet();

		/**
		 * The meta object literal for the '<em><b>Events</b></em>' reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference ALPHABET__EVENTS = eINSTANCE.getAlphabet_Events();

		/**
		 * The meta object literal for the '{@link de.luh.se.fmse.lts.impl.EventImpl <em>Event</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see de.luh.se.fmse.lts.impl.EventImpl
		 * @see de.luh.se.fmse.lts.impl.LtsPackageImpl#getEvent()
		 * @generated
		 */
		EClass EVENT = eINSTANCE.getEvent();

		/**
		 * The meta object literal for the '{@link de.luh.se.fmse.lts.impl.EventDeclarationsImpl <em>Event Declarations</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see de.luh.se.fmse.lts.impl.EventDeclarationsImpl
		 * @see de.luh.se.fmse.lts.impl.LtsPackageImpl#getEventDeclarations()
		 * @generated
		 */
		EClass EVENT_DECLARATIONS = eINSTANCE.getEventDeclarations();

		/**
		 * The meta object literal for the '<em><b>Declared Events</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference EVENT_DECLARATIONS__DECLARED_EVENTS = eINSTANCE.getEventDeclarations_DeclaredEvents();

		/**
		 * The meta object literal for the '{@link de.luh.se.fmse.lts.impl.StateListToComposedStateMapEntryImpl <em>State List To Composed State Map Entry</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see de.luh.se.fmse.lts.impl.StateListToComposedStateMapEntryImpl
		 * @see de.luh.se.fmse.lts.impl.LtsPackageImpl#getStateListToComposedStateMapEntry()
		 * @generated
		 */
		EClass STATE_LIST_TO_COMPOSED_STATE_MAP_ENTRY = eINSTANCE.getStateListToComposedStateMapEntry();

		/**
		 * The meta object literal for the '<em><b>Key</b></em>' reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference STATE_LIST_TO_COMPOSED_STATE_MAP_ENTRY__KEY = eINSTANCE.getStateListToComposedStateMapEntry_Key();

		/**
		 * The meta object literal for the '<em><b>Value</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference STATE_LIST_TO_COMPOSED_STATE_MAP_ENTRY__VALUE = eINSTANCE.getStateListToComposedStateMapEntry_Value();

	}

} //LtsPackage
