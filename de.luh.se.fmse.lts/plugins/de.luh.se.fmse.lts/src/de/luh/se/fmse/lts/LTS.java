/**
 */
package de.luh.se.fmse.lts;

import org.eclipse.emf.common.util.EList;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>LTS</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link de.luh.se.fmse.lts.LTS#getLtsnetwork <em>Ltsnetwork</em>}</li>
 *   <li>{@link de.luh.se.fmse.lts.LTS#getStates <em>States</em>}</li>
 *   <li>{@link de.luh.se.fmse.lts.LTS#getAlphabet <em>Alphabet</em>}</li>
 *   <li>{@link de.luh.se.fmse.lts.LTS#getStartState <em>Start State</em>}</li>
 * </ul>
 * </p>
 *
 * @see de.luh.se.fmse.lts.LtsPackage#getLTS()
 * @model
 * @generated
 */
public interface LTS extends NamedElement {
	/**
	 * Returns the value of the '<em><b>Ltsnetwork</b></em>' container reference.
	 * It is bidirectional and its opposite is '{@link de.luh.se.fmse.lts.LTSNetwork#getLtss <em>Ltss</em>}'.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Ltsnetwork</em>' container reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Ltsnetwork</em>' container reference.
	 * @see #setLtsnetwork(LTSNetwork)
	 * @see de.luh.se.fmse.lts.LtsPackage#getLTS_Ltsnetwork()
	 * @see de.luh.se.fmse.lts.LTSNetwork#getLtss
	 * @model opposite="ltss" transient="false"
	 * @generated
	 */
	LTSNetwork getLtsnetwork();

	/**
	 * Sets the value of the '{@link de.luh.se.fmse.lts.LTS#getLtsnetwork <em>Ltsnetwork</em>}' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Ltsnetwork</em>' container reference.
	 * @see #getLtsnetwork()
	 * @generated
	 */
	void setLtsnetwork(LTSNetwork value);

	/**
	 * Returns the value of the '<em><b>States</b></em>' containment reference list.
	 * The list contents are of type {@link de.luh.se.fmse.lts.State}.
	 * It is bidirectional and its opposite is '{@link de.luh.se.fmse.lts.State#getContainingLTS <em>Containing LTS</em>}'.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>States</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>States</em>' containment reference list.
	 * @see de.luh.se.fmse.lts.LtsPackage#getLTS_States()
	 * @see de.luh.se.fmse.lts.State#getContainingLTS
	 * @model opposite="containingLTS" containment="true"
	 * @generated
	 */
	EList<State> getStates();

	/**
	 * Returns the value of the '<em><b>Alphabet</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Alphabet</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Alphabet</em>' containment reference.
	 * @see #setAlphabet(Alphabet)
	 * @see de.luh.se.fmse.lts.LtsPackage#getLTS_Alphabet()
	 * @model containment="true"
	 * @generated
	 */
	Alphabet getAlphabet();

	/**
	 * Sets the value of the '{@link de.luh.se.fmse.lts.LTS#getAlphabet <em>Alphabet</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Alphabet</em>' containment reference.
	 * @see #getAlphabet()
	 * @generated
	 */
	void setAlphabet(Alphabet value);

	/**
	 * Returns the value of the '<em><b>Start State</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Start State</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Start State</em>' reference.
	 * @see #setStartState(State)
	 * @see de.luh.se.fmse.lts.LtsPackage#getLTS_StartState()
	 * @model
	 * @generated
	 */
	State getStartState();

	/**
	 * Sets the value of the '{@link de.luh.se.fmse.lts.LTS#getStartState <em>Start State</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Start State</em>' reference.
	 * @see #getStartState()
	 * @generated
	 */
	void setStartState(State value);

} // LTS
