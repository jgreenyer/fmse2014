/**
 */
package de.luh.se.fmse.lts.impl;

import de.luh.se.fmse.lts.Event;
import de.luh.se.fmse.lts.EventDeclarations;
import de.luh.se.fmse.lts.LtsPackage;

import java.util.Collection;

import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

import org.eclipse.emf.ecore.util.EObjectContainmentEList;
import org.eclipse.emf.ecore.util.InternalEList;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Event Declarations</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * <ul>
 *   <li>{@link de.luh.se.fmse.lts.impl.EventDeclarationsImpl#getDeclaredEvents <em>Declared Events</em>}</li>
 * </ul>
 * </p>
 *
 * @generated
 */
public class EventDeclarationsImpl extends MinimalEObjectImpl.Container implements EventDeclarations {
	/**
	 * The cached value of the '{@link #getDeclaredEvents() <em>Declared Events</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getDeclaredEvents()
	 * @generated
	 * @ordered
	 */
	protected EList<Event> declaredEvents;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected EventDeclarationsImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return LtsPackage.Literals.EVENT_DECLARATIONS;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<Event> getDeclaredEvents() {
		if (declaredEvents == null) {
			declaredEvents = new EObjectContainmentEList<Event>(Event.class, this, LtsPackage.EVENT_DECLARATIONS__DECLARED_EVENTS);
		}
		return declaredEvents;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case LtsPackage.EVENT_DECLARATIONS__DECLARED_EVENTS:
				return ((InternalEList<?>)getDeclaredEvents()).basicRemove(otherEnd, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case LtsPackage.EVENT_DECLARATIONS__DECLARED_EVENTS:
				return getDeclaredEvents();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case LtsPackage.EVENT_DECLARATIONS__DECLARED_EVENTS:
				getDeclaredEvents().clear();
				getDeclaredEvents().addAll((Collection<? extends Event>)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case LtsPackage.EVENT_DECLARATIONS__DECLARED_EVENTS:
				getDeclaredEvents().clear();
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case LtsPackage.EVENT_DECLARATIONS__DECLARED_EVENTS:
				return declaredEvents != null && !declaredEvents.isEmpty();
		}
		return super.eIsSet(featureID);
	}

} //EventDeclarationsImpl
